<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsdtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usdts', function (Blueprint $table) {
            $table->id();
            $table->double('harga_jual', 20, 2);
            $table->double('harga_beli', 20, 2);
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usdts');
    }
}
