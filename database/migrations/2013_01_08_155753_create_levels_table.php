<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('direct_bonus');
            $table->bigInteger('sponsor_request');
            $table->bigInteger('bot_request');
            $table->bigInteger('depo_minimum');
            $table->bigInteger('wd_maximum');
            $table->bigInteger('profit_share');
            $table->string('lvl');
            $table->string('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels');
    }
}
