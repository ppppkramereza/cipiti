<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_accounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->double('amount', 50, 2);
            $table->foreignId('bank_id')->references('id')->on('tabel_banks');
            $table->double('kurs_jual', 50, 2);
            $table->integer('kode_unik');
            $table->foreignId('status')->references('id')->on('status');
            $table->timestamp('batas_pembayaran');
            $table->foreignUuid('image_id')->nullable()->references('id')->on('images');
            $table->date('bot_exp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_accounts');
    }
}
