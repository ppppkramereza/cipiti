<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emoneys', function (Blueprint $table) {
            $table->id();
            $table->string('card_number');
            $table->string('holder/upline');
            $table->string('used/downline');
            $table->string('qr_generate');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emoneys');
    }
}
