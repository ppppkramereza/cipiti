<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeuntungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keuntungans', function (Blueprint $table) {
            $table->id();
            $table->double('tambah', 50, 2);
            $table->double('nilai_awal', 50, 2);
            $table->double('nilai_akhir', 50, 2);
            $table->foreignId('fromm')->references('id')->on('list_coins');
            $table->foreignId('to')->references('id')->on('list_coins');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keuntungans');
    }
}
