<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClolumnLastActionAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_mt5', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('activation_accounts', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('aktivasis', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('bank_kodes', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('bank_users', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('bot_prices', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('contacts', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('downlines', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('history_mootas', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('history_types', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('images', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('keuntungans', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('levels', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('metode_pembayarans', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('mutasi_history', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('mutasi_mootas', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('phone_countries', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('status', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('tabel_banks', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('tacs', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('topups', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('total_bawahs', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('type_notifications', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('uplines', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('user_buys', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('version_apps', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('walletmt5s', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('walletmt5_historys', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('wallets', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('wallet_historys', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
        Schema::table('withdraws', function (Blueprint $table) {
            $table->foreignId('last_action')->nullable()->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('all', function (Blueprint $table) {
            //
        });
    }
}
