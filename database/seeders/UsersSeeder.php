<?php

namespace Database\Seeders;

use App\Models\Downline;
use App\Models\Upline;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Wallet;
use App\Models\Walletmt5;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::create([
            'name' => 'Owner',
            'email' => 'Owner@email.com',
            'country_code' => '+62',
            'phone' => '2',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '7',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('Owner');
        $user = User::create([
            'name' => 'Dev',
            'email' => 'Dev',
            'country_code' => '+62',
            'phone' => '1',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '7',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('Super Admin');
        $user = User::create([
            'name' => 'Directure',
            'email' => 'Directure@email.com',
            'country_code' => '+62',
            'phone' => '3',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '1',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Manager',
            'email' => 'Manager@email.com',
            'country_code' => '+62',
            'phone' => '4',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '1',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('Leader');

        $user = User::create([
            'name' => 'Cashier',
            'email' => 'Cashier@email.com',
            'country_code' => '+62',
            'phone' => '5',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '1',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Customer',
            'email' => 'Customer@email.com',
            'country_code' => '+62',
            'phone' => '6',
            'refferal' => Str::random(6),
            'status' => '1',
            'level_id' => '1',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            // 'modal_user' => 0,
            // 'total_robot' => 0,
        ]);
        Wallet::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        Walletmt5::create([
            'user_id' => $user->id,
            'balance' => 0,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);
        $user->assignRole('User');
        Upline::create([
            'user_id' => 6,
            'upline_id' => 1
        ]);
        Upline::create([
            'user_id' => 5,
            'upline_id' => 1
        ]);
        Upline::create([
            'user_id' => 4,
            'upline_id' => 1
        ]);
        Upline::create([
            'user_id' => 3,
            'upline_id' => 1
        ]);
        Upline::create([
            'user_id' => 2,
            'upline_id' => 1
        ]);
        Downline::create([
            'downline_id' => 6,
            'user_id' => 1,
        ]);
        Downline::create([
            'downline_id' => 5,
            'user_id' => 1,
        ]);
        Downline::create([
            'downline_id' => 4,
            'user_id' => 1,
        ]);
        Downline::create([
            'downline_id' => 3,
            'user_id' => 1,
        ]);
        Downline::create([
            'downline_id' => 2,
            'user_id' => 1,
        ]);
    }
}
