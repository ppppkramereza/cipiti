<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            StatusSeeder::class,
            RolesSeeder::class,
            LevelsSeeder::class,
            UsdtsSeeder::class,
            UsersSeeder::class,
            HistorytypeSeeder::class,
            PhoneCountrieSeeder::class,
            BotpriceSeeder::class,
            MetodepembayaranSeeder::class,
            TabelbankSeeder::class,
            BankkodeSeeder::class,
            SettingSeeder::class,
            TacSeeder::class,
            AlamatSeeder::class,
            KursSeeder::class,
            ListcoinSeeder::class,
            TotalbawahSeeder::class,
        ]);
    }
}
