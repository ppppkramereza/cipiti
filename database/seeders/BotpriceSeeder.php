<?php

namespace Database\Seeders;

use App\Models\Botprice;
use Illuminate\Database\Seeder;

class BotpriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Botprice::create([
            'name' => 'CIPTI TAG',
            'price' => '100',
            'description' =>
            '<h1>CIPITI</h1>'
        ]);
    }
}
