<?php

namespace Database\Seeders;

use App\Models\Usdts;
use Illuminate\Database\Seeder;

class UsdtsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usdts::create([
            'harga_jual' => 10000,
            'harga_beli' => 10000,
        ]);
    }
}
