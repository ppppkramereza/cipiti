<?php

namespace Database\Seeders;

use App\Models\Alamat;
use GuzzleHttp\Promise\Create;
use Illuminate\Database\Seeder;

class AlamatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Alamat::create([
            'user_id' => 1,
            'alamat' => 'Owner',
            'nohp' => 'Owner',
            'kode_pos' => 'Owner',
            'nama_penerima' => 'Owner',
            'title' => 'Owner',
            'last_action' => 1,
        ]);
    }
}
