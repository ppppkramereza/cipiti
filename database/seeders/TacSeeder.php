<?php

namespace Database\Seeders;

use App\Models\Tac;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class TacSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
        Tac::create([
            'kode_produksi' => $faker->regexify('[A-Z0-9]{5}'),    'tac_1' => $faker->regexify('[A-Z0-9]{5}'),    'tac_2' => $faker->regexify('[A-Z0-9]{5}'),    'tac_3' => $faker->regexify('[A-Z0-9]{5}'),    'tac_4' => $faker->regexify('[A-Z0-9]{5}'),
        ]);
    }
}
