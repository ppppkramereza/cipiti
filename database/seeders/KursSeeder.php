<?php

namespace Database\Seeders;

use App\Models\Kurs;
use Illuminate\Database\Seeder;

class KursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kurs::create([
            'nama' => 'KURS',
            'harga_jual' => '10000',
            'harga_beli' => '10000',
        ]);
    }
}
