<?php

namespace Database\Seeders;

use App\Models\Historytype;
use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
            'name' => 'Sudah Aktif Email',
            'for' => 'User',
        ]);
        Status::create([
            'name' => 'Activation Member',
            'for' => 'User',
        ]);
        Status::create([
            'name' => 'Sudah Aktifasi TAC',
            'for' => 'User',
        ]);
        Status::create([
            'name' => 'Menunggu Pembayaran',
            'for' => 'Activation',
        ]);
        Status::create([
            'name' => 'Berhasil Dibayar',
            'for' => 'Activation',
        ]);
        Status::create([
            'name' => 'Debit',
            'for' => 'Mutasi',
        ]);
        Status::create([
            'name' => 'Debit - Waiting',
            'for' => 'Mutasi',
        ]);
        Status::create([
            'name' => 'Credit',
            'for' => 'Mutasi',
        ]);
        Status::create([
            'name' => 'Credit - Waiting',
            'for' => 'Mutasi',
        ]);
        Status::create([
            'name' => 'Belum Di Bayar',
            'for' => 'Top-Up',
        ]);
        Status::create([
            'name' => 'Pending',
            'for' => 'Top-Up',
        ]);
        Status::create([
            'name' => 'Menunggu Konfirmasi',
            'for' => 'Top-Up',
        ]);
        Status::create([
            'name' => 'Selesai',
            'for' => 'Top-Up',
        ]);
        Status::create([
            'name' => 'Pending',
            'for' => 'Withdraw',
        ]);
        Status::create([
            'name' => 'Selesai',
            'for' => 'Withdraw',
        ]);
        Status::create([
            'name' => 'Wallet',
            'for' => 'Wallet Type',
        ]);
        Status::create([
            'name' => 'Wallet MT5',
            'for' => 'Wallet Type',
        ]);
        Status::create([
            'name' => 'Di Tolak',
            'for' => 'Transaksi',
        ]);
    }
}
