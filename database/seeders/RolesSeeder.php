<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'name' => 'Super Admin',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Admin',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Owner',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'User',
            'guard_name' => 'web',
        ]);
        Roles::create([
            'name' => 'Leader',
            'guard_name' => 'web',
        ]);
    }
}
