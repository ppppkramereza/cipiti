<?php

namespace Database\Seeders;

use App\Models\VersionApp;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VersionApp::create([
            'versi_apps' => 1,
        ]);
    }
}
