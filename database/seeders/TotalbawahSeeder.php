<?php

namespace Database\Seeders;

use App\Models\Totalbawah;
use Illuminate\Database\Seeder;

class TotalbawahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Totalbawah::create([
            'user_baru' => '6',
            'user_up' => '1',
        ]);
        Totalbawah::create([
            'user_baru' => '5',
            'user_up' => '1',
        ]);
        Totalbawah::create([
            'user_baru' => '4',
            'user_up' => '1',
        ]);
        Totalbawah::create([
            'user_baru' => '3',
            'user_up' => '1',
        ]);
        Totalbawah::create([
            'user_baru' => '2',
            'user_up' => '1',
        ]);
    }
}
