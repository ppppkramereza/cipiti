<?php

namespace Database\Seeders;

use App\Models\Tabelbank;
use Illuminate\Database\Seeder;

class TabelbankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tabelbank::create([
            'bank_name' => 'BCA',
            'bank_code' => '008',
            'account_name' => 'PT Cipiti Makmur Bersama',
            'account_number' => '1015077788',
        ]);
    }
}
