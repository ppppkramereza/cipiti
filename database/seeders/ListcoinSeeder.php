<?php

namespace Database\Seeders;

use App\Models\Listcoin;
use Illuminate\Database\Seeder;

class ListcoinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Listcoin::create([
            'name' => 'PITI',
        ]);
        Listcoin::create([
            'name' => 'USDT',
        ]);
    }
}
