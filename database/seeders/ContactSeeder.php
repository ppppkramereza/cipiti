<?php

namespace Database\Seeders;

use App\Models\Contacts;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contacts::create([
            'telp' => '081277288212',
            'alamat' => 'JL Pacar No 2 Desa/Kelurahan Ketabang,
Kecamatan Genteng, Kota Surabaya. Indonesia',
            'email' => 'cs@dailypips.co',
            'twitter' => 'dailypips',
            'facebook' => 'dailypips',
            'youtube' => 'dailypips',
            'instagram' => 'dailypips',
            'website' => 'https://dailypips.co',
        ]);
    }
}
