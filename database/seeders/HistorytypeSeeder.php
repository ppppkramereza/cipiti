<?php

namespace Database\Seeders;

use App\Models\Historytype;
use Illuminate\Database\Seeder;

class HistorytypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Historytype::create([
            'name' => 'Star 1 Reward',
        ]);
        Historytype::create([
            'name' => 'Bonus Profit',
        ]);
        Historytype::create([
            'name' => 'WD',
        ]);
        Historytype::create([
            'name' => 'Bonus Cendol',
        ]);
    }
}
