<?php

namespace Database\Seeders;

use App\Models\Metodepembayaran;
use Illuminate\Database\Seeder;

class MetodepembayaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Metodepembayaran::create([
            'nama' => 'Transfer',
            'deskripsi' => 'Manual Transfer'
        ]);
    }
}
