@extends('layouts.dash')
@section('script-top')
    <title>List Robot</title>


    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ asset('assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content ">
        <div class="row ">
            <!-- /.col -->
            <div class="col-md-1">

                <!-- /.box -->
            </div>
            <div class="col-md-6 ">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bot Detail</h3>
                        <span class="mailbox-read-time pull-right">
                        </span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">

                        <div class="mailbox-read-info">
                            <h3>{{ $data->name }} </h3>
                            <h5>
                                <span class="mailbox-read-time pull-right">{{ $data->created_at }}</span>
                            </h5>
                        </div>
                        <div class="mailbox-read-message ">
                            <?php echo $data->description; ?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>


                </div>
                <!-- /. box -->
            </div>
            <div class="col-md-4">
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pilih Pembayaran</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form" action="{{ route('dashboard.customer.beli-robot.belirobot') }}"
                            method="post">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Metode Pembayaran</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                        tabindex="-1" aria-hidden="true" name="metode_pembayaran">
                                        @foreach ($metode as $metodes)
                                            <option value="{{ $metodes->id }}">{{ $metodes->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pilih Bank</label>
                                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                        tabindex="-1" aria-hidden="true" name="metode_pembayaran">
                                        @foreach ($bank as $banks)
                                            <option value="{{ $banks->id }}">{{ $banks->bank_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Harga Bot</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1"
                                        placeholder="Password" name="" readonly
                                        value="@currency( $data->price * $kurs->harga_jual)">
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <input type="hidden" name="bot_id" value="{{ $data->id }}">
                            <input type="hidden" name="kurs" value="{{ $kurs->harga_jual }}">
                            <div class="box-footer">
                                <button type="submit" class="btn btn-block btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>


                    </form>
                    <!-- /.mailbox-read-message -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
