@extends('layouts.dash')
<title>Dashboard | Manajemen User</title>
@section('content')
     <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Akun MT5
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Manajemen User</a></li>
        <li class="active">Akun MT5</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @php if(Session::get('berhasil')){ @endphp 
      <div class="alert alert-success" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil ditambahkan
      </div> @php } @endphp
      @php if(Session::get('berhasilupdate')){ @endphp 
      <div class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                     Data Berhasil diupdate
      </div> @php } @endphp
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Akun MT5</h3>
              <span class="input-group-btn">
                <a data-toggle="modal" data-target="#modal-tambah"><buttonid="search-btn" class="btn btn-primary"><i class="fa fa-plus"></i>
                </button>Tambah Data</a>
              </span>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Member</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Server</th>
                  <th>IP Server</th>
                  <th>Password Investor</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1 ;
                @endphp
                @foreach ($datas as $dt)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$dt->user->name}}</td>
                  <td>{{$dt->username}}</td>
                  <td>{{$dt->password}}</td>
                  <td>{{$dt->server}}</td>
                  <td>{{$dt->ip_server}}</td>
                  <td>{{$dt->password_investor}}</td>
                  <td>
                  <a class="btn btn-warning" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                      </a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Akun MT5</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.cashier.manajemenuser.akunmt5.store') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="user_id" class="col-sm-2 control-label">Member</label>
                  <div class="col-sm-10">
                    @php
                    $data = DB::select("select * from users a, model_has_roles b where a.id = b.model_id and b.role_id=7 and a.id NOT IN (SELECT user_id FROM account_mt5)");
                    @endphp
                    <!-- <select class="form-control" name="user_id" id="user_id">
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select> -->
                    <select class="form-control select2" name="user_id" id="user_id" style="width: 100%;">
                      <!-- <option selected="selected">Alabama</option> -->
                      @foreach ($data as $dt)
                      <option value="{{$dt->id}}">{{$dt->name}}</option>
                      @endforeach
                    </select>
                  </div> 
                  
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Michael111" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" id="password" name="password" placeholder="mypassword" required="" >
                  </div>
                  <div class="col-sm-3">
                    <input type="checkbox" id="pw1cek"/>Show Password
                  </div>
                </div>
                <div class="form-group">
                  <label for="passwordinv" class="col-sm-2 control-label">Password Investor</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" id="passwordinv" name="passwordinv" placeholder="mypassword" required="" >
                  </div>
                  <div class="col-sm-3">
                    <input type="checkbox" id="pw2cek"/>Show Password
                  </div>
                </div>
                <div class="form-group">
                  <label for="server" class="col-sm-2 control-label">Server</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="server" placeholder="server" name="server" required="" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="server_ip" class="col-sm-2 control-label">IP Server</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="server_ip" placeholder="server_ip" name="server_ip" required="" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
        @foreach ($datas as $dt)
          <div class="modal fade" id="modal-edit{{$dt->id}}" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit Akun MT5 {{$dt->user->name}}</h4>
            </div>
            <div class="modal-body">
             <form class="form-horizontal" action="{{ route('dashboard.cashier.manajemenuser.akunmt5.update') }}" method="post">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael111" required="" value="{{$dt->id}}" >
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Michael111" required="" value="{{$dt->username}}" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="passwordedit{{$dt->id}}" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" id="passwordedit{{$dt->id}}" name="passwordedit{{$dt->id}}" placeholder="mypassword" required="" value="{{$dt->password}}" >
                  </div>
                  <div class="col-sm-3">
                    <input type="checkbox" id="pw1cekedit{{$dt->id}}"/>Show Password
                  </div>
                </div>
                <div class="form-group">
                  <label for="passwordinvedit{{$dt->id}}" class="col-sm-2 control-label">Password Investor</label>
                  <div class="col-sm-7">
                    <input type="password" class="form-control" id="passwordinvedit{{$dt->id}}" name="passwordinvedit{{$dt->id}}" placeholder="mypassword" required="" value="{{$dt->password_investor}}">
                  </div>
                  <div class="col-sm-3">
                    <input type="checkbox" id="pw2cekedit{{$dt->id}}"/>Show Password
                  </div>
                </div>
                <div class="form-group">
                  <label for="server" class="col-sm-2 control-label">Server</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="server" placeholder="server" name="server" required="" value="{{$dt->server}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="server_ip" class="col-sm-2 control-label">IP Server</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="server_ip" placeholder="server_ip" name="server_ip" required="" value="{{$dt->ip_server}}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>
        @endforeach

    </section>
    <!-- /.content -->
    <!-- DataTables -->

@endsection
@section('scripts')
    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
      $(function () {
        $('#table1').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
        $('#table2').DataTable({
          'paging'      : true,
          'lengthChange': true,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : true
        })
      })
    </script>
    <!-- Select2 -->

    <script>
        $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        })
    </script>
    <script src="{{asset('assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script>
    $(document).ready(function(){

        $("#password").focus();

        $("#pw1cek").click(function(){
            if ($("#pw1cek").is(":checked"))
            {
                $("#password").clone()
                .attr("type", "text").insertAfter("#password")
                .prev().remove();


                $("#password").clone()
                .attr("type", "text");
            }
            else
            {
                $("#password").clone()
                .attr("type","password").insertAfter("#password")
                .prev().remove();
            }
        });
    });
    </script>

    <script>
    $(document).ready(function(){

        $("#passwordinv").focus();

        $("#pw2cek").click(function(){
            if ($("#pw2cek").is(":checked"))
            {
                $("#passwordinv").clone()
                .attr("type", "text").insertAfter("#passwordinv")
                .prev().remove();
            }
            else
            {
                $("#passwordinv").clone()
                .attr("type","password").insertAfter("#passwordinv")
                .prev().remove();
            }
        });
    });
    </script>
    @foreach ($datas as $dt)
    <script>
    $(document).ready(function(){

        $("#passwordedit{{$dt->id}}").focus();

        $("#pw1cekedit{{$dt->id}}").click(function(){
            if ($("#pw1cekedit{{$dt->id}}").is(":checked"))
            {
                $("#passwordedit{{$dt->id}}").clone()
                .attr("type", "text").insertAfter("#passwordedit{{$dt->id}}")
                .prev().remove();
            }
            else
            {
                $("#passwordedit{{$dt->id}}").clone()
                .attr("type","password").insertAfter("#passwordedit{{$dt->id}}")
                .prev().remove();
            }
        });
    });
    </script>
    <script>
    $(document).ready(function(){

        $("#passwordinvedit{{$dt->id}}").focus();

        $("#pw2cekedit{{$dt->id}}").click(function(){
            if ($("#pw2cekedit{{$dt->id}}").is(":checked"))
            {
                $("#passwordinvedit{{$dt->id}}").clone()
                .attr("type", "text").insertAfter("#passwordinvedit{{$dt->id}}")
                .prev().remove();
            }
            else
            {
                $("#passwordinvedit{{$dt->id}}").clone()
                .attr("type","password").insertAfter("#passwordinvedit{{$dt->id}}")
                .prev().remove();
            }
        });
    });
    </script>
    @endforeach
@endsection
