@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])


@section('content')
    <!-- Main content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-3 header-title">Checking Muttation</h4>

                            <form action="{{ route('dashboard.dev.mutasi.index.find') }}" method="post">
                                @csrf
                                <div class="mb-3">
                                    <label for="amount" class="form-label">Amount</label>
                                    <input name="amount" type="text" class="form-control" placeholder="Amount" required>
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description</label>
                                    <input name="description" type="text" class="form-control" placeholder="Description">
                                </div>

                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div>
                <!-- end col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

@endsection
