@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <a href="{{ route('dashboard.dev.mutasi.index') }}"><button type="button"
                        class="btn btn-primary">Back</button></a>
                        <br><br>
                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>account_number</th>
                                <th>date</th>
                                <th>description</th>
                                <th>amount</th>
                                <th>type</th>
                                <th>note</th>
                                <th>balance</th>
                                <th>created_at</th>
                                <th>updated_at</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $item)
                        <tr>
                            <td>{{ $item->account_number }}</td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->type }}</td>
                            <td>{{ $item->note }}</td>
                            <td>{{ $item->balance }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->updated_at }}</td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->


@endsection
