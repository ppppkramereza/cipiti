@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Data Mutasi Moota</h4>
                        <p class="text-muted font-14 mb-3">
                        </p>

                        <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>Moota Id</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Balance</th>
                                    <th>Bank Id</th>
                                    <th>Bank Type</th>
                                    <th>Description</th>
                                    <th>Account Number</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($data as $key => $item)
                                    <tr>
                                        {{-- <td>{{ $item->payload }}</td> --}}
                                        <td>{{ $item->id_moota }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->balance }}</td>
                                        <td>{{ $item->bank_id }}</td>
                                        <td>{{ $item->bank_type }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->account_number }}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div> <!-- end row -->


    </div> <!-- container-fluid -->
@endsection
