@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])


@section('content')
    <!-- Main content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-3 header-title">Checking Version</h4>


                            @php
                            $vr = DB::select("select * from version_apps");
                            @endphp
                            @foreach ($vr as $data)
                            <form action="{{ route('dashboard.dev.version.store') }}" method="post">
                                @csrf
                                <div class="mb-3">
                                    <label for="exampleInputEmail1">Versi Apps</label>
                                    <input name="versi_apps" type="text" class="form-control"  value="{{$data->versi_apps}}">
                                </div>


                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </form>
                            @endforeach

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div>
                <!-- end col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

@endsection
