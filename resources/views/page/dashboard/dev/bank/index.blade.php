@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data Bank</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#standard-modal">Tambah Data</button>
                    <br><br>
                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Bank</th>
                                <th>Kode</th>
                                <th>Nama Akun</th>
                                <th>No. Rekening</th>
                                <th>Action</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->bank_name}}</td>
                            <td>{{$dt->bank_code}}</td>
                            <td>{{$dt->account_name}}</td>
                            <td>{{$dt->account_number}}</td>
                            <td><button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modal-edit{{$dt->id}}"><i class="mdi mdi-pencil"></i>
                                 </button>
                                <a href="/dashboard/dev/bank/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="mdi mdi-trash-can"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->

@foreach ($datas as $dt)
<!-- Standard modal content -->
<div id="modal-edit{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalupdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalupdate">Update Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.bank.update') }}" class="parsley-examples" method="post">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Bank<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Bank" class="form-control" id="nama" value="{{$dt->bank_name}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="kode" class="form-label">Kode<span class="text-danger">*</span></label>
                        <input type="text" name="kode" parsley-trigger="change" required placeholder="Enter Bank Code" class="form-control" id="kode" value="{{$dt->bank_code}}"/>
                    </div>

                    <div class="mb-3">
                        <label for="nama_akun" class="form-label">Nama Akun<span class="text-danger">*</span></label>
                        <input type="text" name="nama_akun" parsley-trigger="change" required placeholder="Enter Account Name" class="form-control" id="nama_akun" value="{{$dt->account_name}}"/>
                    </div>

                    <div class="mb-3">
                        <label for="no_rek" class="form-label">No. Rekening<span class="text-danger">*</span></label>
                        <input type="number" name="no_rek" parsley-trigger="change" required placeholder="Enter Account Number" class="form-control" id="no_rek" value="{{$dt->account_number}}"/>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach

<!-- Standard modal content -->
<div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">Tambah Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.bank.store') }}" class="parsley-examples" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Bank<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Bank" class="form-control" id="nama" />
                    </div>
                    <div class="mb-3">
                        <label for="kode" class="form-label">Kode<span class="text-danger">*</span></label>
                        <input type="text" name="kode" parsley-trigger="change" required placeholder="Enter Bank Code" class="form-control" id="kode" />
                    </div>

                    <div class="mb-3">
                        <label for="nama_akun" class="form-label">Nama Akun<span class="text-danger">*</span></label>
                        <input type="text" name="nama_akun" parsley-trigger="change" required placeholder="Enter Account Name" class="form-control" id="nama_akun" />
                    </div>

                    <div class="mb-3">
                        <label for="no_rek" class="form-label">No. Rekening<span class="text-danger">*</span></label>
                        <input type="number" name="no_rek" parsley-trigger="change" required placeholder="Enter Account Number" class="form-control" id="no_rek" />
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection
