@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])


@section('content')
    <!-- Main content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="mb-3 header-title">Bypass Robot</h4>


                            @php
                            $vr = DB::select("select * from version_apps");
                            @endphp
                            @foreach ($vr as $data)
                            <form action="{{ route('api.belirobotbypass') }}" method="post">
                                @csrf
                                <div class="mb-3">
                                    <label for="phone" class="form-label"></label>
                                    <select class="form-select" name="user_id" id="user_id" style="">
                                        @foreach ($users as $dt)
                                        <option value="{{$dt->id}}">{{$dt->name}} - {{$dt->email}}</option>
                                        @endforeach

                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            </form>
                            @endforeach

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div>
                <!-- end col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->

@endsection
