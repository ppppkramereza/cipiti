@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data Kurs</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#standard-modal">Tambah Data</button>
                    <br><br>
                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Harga Beli</th>
                                <th>Harga Jual</th>
                                <th>Action</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->nama}}</td>
                            <td>{{$dt->harga_beli}}</td>
                            <td>{{$dt->harga_jual}}</td>
                            <td><button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modal-edit{{$dt->id}}"><i class="mdi mdi-pencil"></i>
                            </button>
                                <a href="/dashboard/dev/kurs/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="mdi mdi-trash-can"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->
@foreach ($datas as $dt)
<!-- Standard modal content -->
<div id="modal-edit{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalupdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalupdate">Update Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.kurs.update') }}" class="parsley-examples" method="post">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Kurs<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name Kurs" class="form-control" id="nama" value="{{$dt->nama}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="harga_jual" class="form-label">Harga Jual<span class="text-danger">*</span></label>
                        <input type="number" name="harga_jual" parsley-trigger="change" required placeholder="Enter Price" class="form-control" id="harga_jual" value="{{$dt->harga_jual}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="harga_beli" class="form-label">Harga Beli<span class="text-danger">*</span></label>
                        <input type="number" name="harga_beli" parsley-trigger="change" required placeholder="Enter Price" class="form-control" id="harga_beli" value="{{$dt->harga_beli}}"/>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach
<!-- Standard modal content -->
<div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">Tambah Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.kurs.store') }}" class="parsley-examples" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nama Kurs<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name Kurs" class="form-control" id="nama" />
                    </div>
                    <div class="mb-3">
                        <label for="harga_jual" class="form-label">Harga Jual<span class="text-danger">*</span></label>
                        <input type="number" name="harga_jual" parsley-trigger="change" required placeholder="Enter Price" class="form-control" id="harga_jual" />
                    </div>
                    <div class="mb-3">
                        <label for="harga_beli" class="form-label">Harga Beli<span class="text-danger">*</span></label>
                        <input type="number" name="harga_beli" parsley-trigger="change" required placeholder="Enter Price" class="form-control" id="harga_beli" />
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
