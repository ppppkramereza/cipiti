@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">History Topup</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>

                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Member</th>
                                <th>Bank</th>
                                <th>Nama Akun</th>
                                <th>Amount (USDT)</th>
                                <th>Amount (IDR)</th>
                                <th>Status</th>
                                <th>Waktu</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                            $usdt = 0 ;
                                $getusdt = DB::select("select * from kurs");
                                    foreach ($getusdt as $keyyy ) {
                                        $usdt = $keyyy->harga_jual;
                                    }
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->nama_user}}</td>
                            <td>{{$dt->nama_bank}}</td>
                            <td>{{$dt->nama_akun}}</td>
                            <td>$ {{$dt->amount}}</td>
                            <td>Rp. {{round($dt->amount * $usdt,1)}}</td>
                            @if($dt->status == 18)
                              <td><center><a class="btn btn-danger" >DITOLAK
                                </a></center></td>
                              @else
                            <td><center><a class="btn btn-success" >SELESAI
                              </a></center></td>
                              @endif
                            <td>{{$dt->created_at}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->




@endsection
