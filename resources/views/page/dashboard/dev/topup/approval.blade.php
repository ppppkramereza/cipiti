@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Approval Topup</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>

                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Member</th>
                                <th>Bank</th>
                                <th>Amount (IDR)</th>
                                <th>Amount (USDT)</th>
                                <th>Kode Unik</th>
                                <th>Status</th>
                                <th>Waktu</th>
                                <th>Action</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                            $usdt = 0 ;
                                $getusdt = DB::select("select * from kurs");
                                    foreach ($getusdt as $keyyy ) {
                                        $usdt = $keyyy->harga_jual;
                                    }
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->nama_user}}</td>
                            <td>{{$dt->nama_bank}}</td>
                            <td>Rp. {{ number_format($dt->amount,0, ',' , '.')}},-
                            </td>
                            <td> ${{round(($dt->amount - $dt->unik)/$usdt, 2)}}</td>
                            <td>{{$dt->unik}}</td>
                            <td>{{$dt->nama_status}}</td>
                            <td>{{$dt->created_at}}</td>
                            <td><!-- <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                                </a>  -->
                            <!-- <a href="" class="btn btn-danger btn-xs" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                            </a> -->
                            <!--
                            <a href="/dashboard/dev/topup/approve{{$dt->id}}" class="btn btn-primary " onclick="return(confirm('Approve Topup {{$dt->nama_user}}?'));"><i class="fa fa-check"></i> -->
                            <a data-toggle="modal" data-target="#modal-detail{{$dt->id}}" class="btn btn-primary " <i class="fa fa-eye"></i> Detail
                            </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->


@foreach ($datas as $dt)
<!-- Standard modal content -->
<div id="modal-detail{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalupdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalupdate">Detail Topup Member</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.topup.approve') }}" class="parsley-examples" method="post">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Lengkap<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name Level" class="form-control" id="nama" value="{{$dt->nama_user}}" readonly=""/>
                    </div>
                    <div class="mb-3">
                        <label for="direct_bonus" class="form-label">Bank<span class="text-danger">*</span></label>
                        <input type="text" name="email" parsley-trigger="change" required placeholder="Enter Direct Bonus" class="form-control" id="email" value="{{$dt->nama_bank}}" readonly=""/>
                    </div>
                    <div class="mb-3">
                        <label for="sponsor_request" class="form-label">Nominal<span class="text-danger">*</span></label>
                        <input type="text" name="nominal" parsley-trigger="change" required placeholder="Enter Sponsor Request" class="form-control" id="nominal" value="Rp. {{ number_format($dt->amount,0, ',' , '.')}},-" readonly=""/>
                        <input type="text" name="nominal" parsley-trigger="change" required placeholder="Enter Sponsor Request" class="form-control" id="nominal" value="${{round(($dt->amount - $dt->unik)/$usdt, 2)}}" readonly=""/>
                    </div>

                    <div class="mb-3">
                        <label for="bot_request" class="form-label">Status<span class="text-danger">*</span></label>
                        <input type="text" name="status" parsley-trigger="change" required placeholder="Enter Bot Request" class="form-control" id="status" value="{{$dt->nama_status}}" readonly=""/>
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach


@endsection
