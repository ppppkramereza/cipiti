@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">History Topup</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>

                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Member</th>
                                <th>Bank</th>
                                <th>Amount (IDR)</th>
                                <th>Amount (USDT)</th>
                                <th>Kode Unik</th>
                                <th>Status</th>
                                <th>Waktu</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                            $usdt = 0 ;
                                $getusdt = DB::select("select * from kurs");
                                    foreach ($getusdt as $keyyy ) {
                                        $usdt = $keyyy->harga_jual;
                                    }
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->nama_user}}</td>
                            <td>{{$dt->nama_bank}}</td>
                            <td>Rp. {{ number_format($dt->amount,0, ',' , '.')}},-
                            </td>
                            <td>$ {{round(($dt->amount - $dt->unik)/$usdt, 2)}}</td>
                            <td>{{$dt->unik}}</td>

                              @if($dt->status == 18)
                              <td><center><a class="btn btn-danger" >DITOLAK
                                </a></center></td>
                              @else if ($dt->status == 13)
                              <td><center><a class="btn btn-success" >SELESAI
                                </a></center></td>
                              @endif

                            <td>{{$dt->created_at}}</td><!--
                            <td><a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-edit{{$dt->id}}"><i class="fa fa-pencil"></i>
                                </a>
                                <a href="" class="btn btn-danger btn-xs" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="fa fa-trash"></i>
                                </a>
                            </td> -->
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->




@endsection
