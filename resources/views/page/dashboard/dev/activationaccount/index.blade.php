@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data Activation Account</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Member</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Kurs Jual</th>
                                <th>Kode Unik</th>
                                <th>Status</th>
                                <th>Batas Pembayaran</th>
                                <th>Waktu Approve</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{ $item->nama_user }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->nama_bank }}</td>
                            <td>{{ $item->kurs_jual }}</td>
                            <td>{{ $item->kode_unik }}</td>
                            <td>{{ $item->nama_status }}</td>
                            <td>{{ $item->batas_pembayaran }}</td>
                            <td>{{ $item->updated_at }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->






@endsection
