@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Activation Account Approval</h4>
                        <p class="text-muted font-14 mb-3">
                        </p>
                        <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Member</th>
                                    <th>Amount</th>
                                    <th>Bank</th>
                                    <th>Wallet From</th>
                                    <th>Kurs Jual</th>
                                    <th>Kode Unik</th>
                                    <th>Status</th>
                                    <th>Batas Pembayaran</th>
                                    <th>Bukti Bayar</th>
                                    <th>action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($datas as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->nama_user }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->nama_bank }}</td>
                                        <td>{{ $item->bank_id }}</td>
                                        <td>{{ $item->kurs_jual }}</td>
                                        <td>{{ $item->kode_unik }}</td>
                                        <td>{{ $item->nama_status }}</td>
                                        <td>{{ $item->batas_pembayaran }}</td>
                                        @if ($item->image_id = null)
                                            <td>Belum Upload</td>
                                        @else
                                            <td><a href="{{ $item->gambar }}" target="_BLANK"><img
                                                        src="{{ $item->gambar }}" width="60"></a></td>
                                        @endif
                                        <td>
                                            @if ($item->bank_id != null)
                                                <a href="/dashboard/dev/activation-account/approve{{ $item->id }}"
                                                    class="btn btn-warning "
                                                    onclick="return(confirm('Approve Akun {{ $item->nama_user }}?'));"><i
                                                        class="fa fa-check"></i>
                                                @else
                                                    <a href="/dashboard/dev/activation-account/approve{{ $item->id }}"
                                                        class="btn btn-primary "
                                                        onclick="return(confirm('Approve Akun {{ $item->nama_user }}?'));"><i
                                                            class="fa fa-check"></i>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div> <!-- end row -->


    </div> <!-- container-fluid -->
@endsection
