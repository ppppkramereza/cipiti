@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Data Bank</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#standard-modal">Tambah Data</button>
                    <br><br>
                    <table id="datatable" class="table table-bordered dt-responsive table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Bank</th>
                                <th>Kode</th>
                                <th>Member</th>
                                <th>Nama Akun</th>
                                <th>No. Rekening</th>
                                <th>Action</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->bank->nama_bank}}</td>
                            <td>{{$dt->bank->kode_bank}}</td>
                            <td>{{$dt->user->name}}</td>
                            <td>{{$dt->account_name}}</td>
                            <td>{{$dt->bank_rek}}</td>
                            <td><button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modal-edit{{$dt->id}}"><i class="mdi mdi-pencil"></i>
                            </button>
                                <a href="/dashboard/dev/member-bank/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="mdi mdi-trash-can"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->
@foreach ($datas as $dt)
<!-- Standard modal content -->
<div id="modal-edit{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalupdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalupdate">Update Data {{$dt->name}}</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">



                <form action="{{ route('dashboard.dev.member-bank.update') }}" class="parsley-examples" method="post">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                    <div class="mb-3">
                        <label for="bank" class="form-label">Bank</label>
                        <select class="form-select" id="bank_id" name="bank_id">
                            @foreach ($datab as $dts)
                            <option value="{{$dts->id}}">{{$dts->nama_bank}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="nama_akun" class="form-label">Nama Akun<span class="text-danger">*</span></label>
                        <input type="text" name="nama_akun" parsley-trigger="change" required placeholder="Enter Nama Akun" class="form-control" id="nama_akun" value="{{$dt->account_name}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="no_rekening" class="form-label">No. Rekening<span class="text-danger">*</span></label>
                        <input type="number" name="no_rek" parsley-trigger="change" required placeholder="Enter Rekening" class="form-control" id="no_rek" value="{{$dt->bank_rek}}"/>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach

<div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">Tambah Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.member-bank.store') }}" class="parsley-examples" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="member" class="form-label">Member</label>
                        <select class="form-select" id="user_id" name="user_id">
                            @foreach ($data as $dta)
                            <option value="{{$dta->id}}">{{$dta->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="bank" class="form-label">Bank</label>
                        <select class="form-select" id="bank_id" name="bank_id">
                            @foreach ($datab as $dts)
                            <option value="{{$dts->id}}">{{$dts->nama_bank}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="nama_akun" class="form-label">Nama Akun<span class="text-danger">*</span></label>
                        <input type="text" name="nama_akun" parsley-trigger="change" required placeholder="Enter Nama Akun" class="form-control" id="nama_akun" />
                    </div>
                    <div class="mb-3">
                        <label for="no_rekening" class="form-label">No. Rekening<span class="text-danger">*</span></label>
                        <input type="number" name="no_rek" parsley-trigger="change" required placeholder="Enter Rekening" class="form-control" id="no_rek" />
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
