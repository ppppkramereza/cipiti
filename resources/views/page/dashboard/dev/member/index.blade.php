@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <h4 class="mt-0 header-title">Data Member</h4>
                        <p class="text-muted font-14 mb-3">
                        </p>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#standard-modal">Tambah Data</button>

                        <br><br>
                        <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap"
                            style="width: 100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Telp</th>
                                    <th>Level</th>
                                    <th>Status</th>
                                    <th>Modal User (USDT)</th>
                                    <th>Saldo Wallet Biasa(USDT)</th>
                                    <th>Tgl Daftar</th>
                                    <th>Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($datas as $dt)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $dt->name }}</td>
                                        <td>{{ $dt->email }}</td>
                                        <td>{{ $dt->country_code }}{{ $dt->phone }}</td>
                                        <td>{{ $dt->leveluser->name }}</td>
                                        <td>{{ $dt->statususer->name }}</td>
                                        <td>{{ $dt->modal_user }}</td>
                                        <td>{{ $dt->wallet->balance }}</td>
                                        <td>{{ $dt->created_at }}</td>
                                        <td><button type="button" class="btn btn-warning" data-bs-toggle="modal"
                                                data-bs-target="#modal-edit{{ $dt->id }}"><i
                                                    class="mdi mdi-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                                                data-bs-target="#modal-upline{{ $dt->id }}"><i
                                                    class="mdi mdi-arrow-decision"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                                                data-bs-target="#modal-password{{ $dt->id }}"><i
                                                    class="mdi mdi-lock-check-outline"></i>
                                            </button>
                                            <a href="/dashboard/dev/user/delete{{ $dt->id }}" class="btn btn-danger"
                                                onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i
                                                    class="mdi mdi-trash-can"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div> <!-- end row -->


    </div> <!-- container-fluid -->
    @foreach ($datas as $dt)
        <!-- Standard modal content -->
        <div id="modal-edit{{ $dt->id }}" class="modal fade" tabindex="-1" role="dialog"
            aria-labelledby="modalupdate" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalupdate">Update Data</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">


                        <form action="{{ route('dashboard.dev.member.update') }}" class="parsley-examples" method="post">
                            @csrf
                            <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada"
                                value="{{ $dt->id }}">
                                <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="profil" required="" >
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama Lengkap<span
                                        class="text-danger">*</span></label>
                                <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name"
                                    class="form-control" id="nama" value="{{ $dt->name }}" />
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                                <input type="text" name="email" parsley-trigger="change" required placeholder="Enter Email"
                                    class="form-control" id="email" value="{{ $dt->email }}" />
                            </div>


                            <div class="mb-3">
                                <label for="phone" class="form-label">Phone<span class="text-danger">*</span></label>
                                <select class="form-select" name="country_code" id="country_code" style="width: 75px;">
                                    @php
                                        $country = DB::select('select * from phone_countries');
                                    @endphp
                                    @foreach ($country as $dts)
                                        @if ($dts->iso_code == $dt->country_code)
                                            <option value="{{ $dts->iso_code }}" selected="">{{ $dts->iso_code }}
                                            </option>
                                        @else
                                            if ($dts ->iso_code != $dt->country_code)
                                            <option value="{{ $dts->iso_code }}">{{ $dts->iso_code }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <input type="text" name="phone" parsley-trigger="change" required placeholder="Enter Phone"
                                    class="form-control" id="phone" value="{{ $dt->phone }}" />
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endforeach

    @foreach ($datas as $dt)
    <div id="modal-upline{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Edit Upline Member {{$dt->name}}</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <form action="{{ route('dashboard.dev.member.update') }}" class="parsley-examples" method="post">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                        <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="upline" required="" >
                        @php
                        $user = DB::select("select * from users");
                        $upline = DB::select("select * from uplines where user_id='$dt->id'");
                        $id_upline = "";
                            foreach($upline as $up){
                                $id_upline = $up->upline_id;
                            }
                        @endphp

                        <div class="mb-3">
                            <label for="phone" class="form-label">Pilih Upline<span
                                    class="text-danger">*</span></label>
                            <select class="form-select" name="upline" id="upline" style="width: 300px;">
                                @foreach ($user as $dtaa)
                                    @if ($id_upline == $dtaa->id)
                                    <option value="{{$dtaa->id}}" selected="">{{$dtaa->name}}</option>
                                    @else if ($id_upline != $dtaa->id)
                                    <option value="{{$dtaa->id}}" >{{$dtaa->name}}</option>
                                    @endif
                                @endforeach
                            </select>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endforeach
    @foreach ($datas as $dt)
    <div id="modal-password{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Edit Password Member {{$dt->name}}</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <form action="{{ route('dashboard.dev.member.update') }}" class="parsley-examples" method="post">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" placeholder="Michael Sumadi" value="{{$dt->id}}" required="" >
                        <input type="hidden" class="form-control" id="mode" name="mode" placeholder="Michael Sumadi" value="password" required="" >


                        <div class="mb-3">
                            <label for="password" class="form-label">Pilih Upline<span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="MyPassword" required="" >
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endforeach
    <!-- Standard modal content -->
    <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">Tambah User</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <form action="{{ route('dashboard.dev.member.store') }}" class="parsley-examples" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Lengkap<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name"
                                class="form-control" id="nama" />
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                            <input type="text" name="email" parsley-trigger="change" required placeholder="Enter Email"
                                class="form-control" id="email" />
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="password" parsley-trigger="change" required
                                placeholder="Enter Password" class="form-control" id="password" />
                        </div>
                        <div class="mb-3">
                            <label for="refferal" class="form-label">Refferal<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="refferal" parsley-trigger="change" required
                                placeholder="Enter Refferal" class="form-control" id="refferal" />
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone<span @php
                                $data = DB::select('select * from phone_countries');
                            @endphp
                                    class="text-danger">*</span></label>
                            <select class="form-select" name="country_code" id="country_code" style="width: 75px;">
                                @foreach ($data as $dt)
                                    <option value="{{ $dt->iso_code }}">{{ $dt->iso_code }}</option>
                                @endforeach

                            </select>
                            <input type="text" name="phone" parsley-trigger="change" required placeholder="Enter Phone"
                                class="form-control" id="phone" style="width: 250px"/>

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- <div id="modaltambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mylargemodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mylargemodalLabel">Tambah Modal</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <form action="{{ route('dashboard.dev.topupmt5.tambahmodal') }}" class="parsley-examples" method="post">
                        @csrf
                        <div id="basicwizard">

                            <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3" style="width: 75%;margin-left: 80px">
                                <li class="nav-item">
                                    <a href="#basictab1" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <i class="mdi mdi-cash"></i>
                                        <span class="d-none d-sm-inline">USD</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#basictab2" data-bs-toggle="tab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <i class="mdi mdi-cash"></i>
                                        <span class="d-none d-sm-inline">IDR</span>
                                    </a>
                                </li>

                            </ul>

                            <div class="tab-content b-0 mb-0 pt-0">
                                <div class="tab-pane" id="basictab1">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row mb-3">
                                                <label for="namamember" class="form-label">Nama Member</label>
                                                        @php
                                                        $data = DB::select("select * from users");
                                                        @endphp
                                                <select class="form-select" name="id" id="id" style="width: 85%;margin-left: 120px" >
                                                    @foreach ($data as $dt)
                                                    <option value="{{$dt->id}}">{{$dt->name}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-3 col-form-label" for="modal"> Modal User</label>
                                                <div class="col-md-9">
                                                    <input type="number" id="modal" name="modal" class="form-control">
                                                </div>
                                            </div>


                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>

                                <div class="tab-pane" id="basictab2">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row mb-3">
                                                <label for="namamember" class="form-label">Nama Member</label>
                                                        @php
                                                        $data = DB::select("select * from users");
                                                        @endphp
                                                <select class="form-select" name="id" id="id" style="width: 85%;margin-left: 120px">
                                                    @foreach ($data as $dt)
                                                    <option value="{{$dt->id}}">{{$dt->name}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="row mb-3">
                                                <label class="col-md-3 col-form-label" for="modalrupiah"> Modal User</label>
                                                <div class="col-md-9">
                                                    <input type="number" id="modalrupiah" name="modalrupiah" class="form-control">
                                                </div>
                                            </div>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->
                                </div>




                            </div> <!-- tab-content -->
                        </div> <!-- end #basicwizard-->


                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> --}}
@endsection
