@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="mt-0 header-title">Data Level</h4>
                    <p class="text-muted font-14 mb-3">
                    </p>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#standard-modal">Tambah Data</button>
                    <br><br>
                    <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Direct Bonus</th>
                                <th>Sponsor Request</th>
                                <th>Bot Request</th>
                                <th>Min Depo</th>
                                <th>Max WD</th>
                                <th>Profit Share</th>
                                <th>Lvl</th>
                                <th>Action</th>
                              </tr>
                        </thead>


                        <tbody>
                        @php
                            $i = 1 ;
                        @endphp
                        @foreach ($datas as $dt)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$dt->name}}</td>
                            <td>{{$dt->direct_bonus}}</td>
                            <td>{{$dt->sponsor_request}}</td>
                            <td>{{$dt->bot_request}}</td>
                            <td>{{$dt->depo_minimum}}</td>
                            <td>{{$dt->wd_maximum}}</td>
                            <td>{{$dt->profit_share}}</td>
                            <td>{{$dt->lvl}}</td>
                            <td><button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modal-edit{{$dt->id}}"><i class="mdi mdi-pencil"></i>
                            </button>
                                <a href="/dashboard/dev/level/delete{{$dt->id}}" class="btn btn-danger" onclick="return(confirm('Apakah Data ini Akan dihapus?'));"><i class="mdi mdi-trash-can"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->
@foreach ($datas as $dt)
<!-- Standard modal content -->
<div id="modal-edit{{$dt->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalupdate" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalupdate">Update Data</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.level.update') }}" class="parsley-examples" method="post">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Level<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name Level" class="form-control" id="nama" value="{{$dt->name}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="direct_bonus" class="form-label">Direct Bonus<span class="text-danger">*</span></label>
                        <input type="number" name="direct_bonus" parsley-trigger="change" required placeholder="Enter Direct Bonus" class="form-control" id="direct_bonus" value="{{$dt->direct_bonus}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="sponsor_request" class="form-label">Sponsor Request<span class="text-danger">*</span></label>
                        <input type="number" name="sponsor_request" parsley-trigger="change" required placeholder="Enter Sponsor Request" class="form-control" id="sponsor_request" value="{{$dt->sponsor_request}}"/>
                    </div>

                    <div class="mb-3">
                        <label for="bot_request" class="form-label">Bot Request<span class="text-danger">*</span></label>
                        <input type="number" name="bot_request" parsley-trigger="change" required placeholder="Enter Bot Request" class="form-control" id="bot_request" value="{{$dt->bot_request}}"/>
                    </div>

                    <div class="mb-3">
                        <label for="depo_minimum" class="form-label">Depo Minimum<span class="text-danger">*</span></label>
                        <input type="number" name="depo_minimum" parsley-trigger="change" required placeholder="Enter Depo Minimum" class="form-control" id="depo_minimum" value="{{$dt->depo_minimum}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="wd_maximum" class="form-label">WD Maximum<span class="text-danger">*</span></label>
                        <input type="number" name="wd_maximum" parsley-trigger="change" required placeholder="Enter WD Maximum" class="form-control" id="wd_maximum" value="{{$dt->wd_maximum}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="profit_share" class="form-label">Profit Share<span class="text-danger">*</span></label>
                        <input type="number" name="profit_share" parsley-trigger="change" required placeholder="Enter Profit Share" class="form-control" id="profit_share" value="{{$dt->profit_share}}"/>
                    </div>
                    <div class="mb-3">
                        <label for="lvl" class="form-label">Level</label>
                        <select class="form-select" id="lvl" name="lvl">
                            <option value="low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                        </select>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach
<!-- Standard modal content -->
<div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="standard-modalLabel">Tambah Level</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <form action="{{ route('dashboard.dev.level.store') }}" class="parsley-examples" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Level<span class="text-danger">*</span></label>
                        <input type="text" name="nama" parsley-trigger="change" required placeholder="Enter Name Level" class="form-control" id="nama" />
                    </div>
                    <div class="mb-3">
                        <label for="direct_bonus" class="form-label">Direct Bonus<span class="text-danger">*</span></label>
                        <input type="text" name="direct_bonus" parsley-trigger="change" required placeholder="Enter Direct Bonus" class="form-control" id="direct_bonus" />
                    </div>
                    <div class="mb-3">
                        <label for="sponsor_request" class="form-label">Sponsor Request<span class="text-danger">*</span></label>
                        <input type="text" name="sponsor_request" parsley-trigger="change" required placeholder="Enter Sponsor Request" class="form-control" id="sponsor_request" />
                    </div>

                    <div class="mb-3">
                        <label for="bot_request" class="form-label">Bot Request<span class="text-danger">*</span></label>
                        <input type="text" name="bot_request" parsley-trigger="change" required placeholder="Enter Bot Request" class="form-control" id="bot_request" />
                    </div>

                    <div class="mb-3">
                        <label for="depo_minimum" class="form-label">Depo Minimum<span class="text-danger">*</span></label>
                        <input type="number" name="depo_minimum" parsley-trigger="change" required placeholder="Enter Depo Minimum" class="form-control" id="depo_minimum" />
                    </div>
                    <div class="mb-3">
                        <label for="wd_maximum" class="form-label">WD Maximum<span class="text-danger">*</span></label>
                        <input type="number" name="wd_maximum" parsley-trigger="change" required placeholder="Enter WD Maximum" class="form-control" id="wd_maximum" />
                    </div>
                    <div class="mb-3">
                        <label for="profit_share" class="form-label">Profit Share<span class="text-danger">*</span></label>
                        <input type="number" name="profit_share" parsley-trigger="change" required placeholder="Enter Profit Share" class="form-control" id="profit_share" />
                    </div>
                    <div class="mb-3">
                        <label for="lvl" class="form-label">Level</label>
                        <select class="form-select" id="lvl" name="lvl">
                            <option value="low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                        </select>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
