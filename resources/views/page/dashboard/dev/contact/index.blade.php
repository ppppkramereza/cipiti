@extends('layouts.dash',['title'=>'Cipiti - Dashboard'])

@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

                    <h4 class="mb-3 header-title">Data Contact</h4>
                    @foreach ($datas as $dt)
                    <form class="form-horizontal" action="{{ route('dashboard.dev.contact.update') }}" method="post">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" placeholder="Bank Persada" value="{{$dt->id}}">
                        <div class="row mb-3">
                            <label for="inputEmail3" class="col-4 col-xl-3 col-form-label">Facebook</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Email"  value="{{$dt->facebook}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword3" class="col-4 col-xl-3 col-form-label">Twitter</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Password" value="{{$dt->twitter}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Instagram</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Retype Password" value="{{$dt->instagram}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Youtube</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="youtube" name="youtube" placeholder="Retype Password" value="{{$dt->youtube}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Website</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="website" name="website" placeholder="Retype Password" value="{{$dt->website}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Telepon</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="telp" name="telp" placeholder="Retype Password" value="{{$dt->telp}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Alamat</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Retype Password" value="{{$dt->alamat}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword5" class="col-4 col-xl-3 col-form-label">Email</label>
                            <div class="col-8 col-xl-9">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Retype Password" value="{{$dt->email}}">
                            </div>
                        </div>

                        <div class="justify-content-end row">
                            <div class="col-8 col-xl-9">
                                <button type="submit" class="btn btn-info waves-effect waves-light">Sign in</button>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>  <!-- end card-body -->
            </div>  <!-- end card -->
        </div>  <!-- end col -->

    </div>
    </div> <!-- end row -->


</div> <!-- container-fluid -->
@endsection
