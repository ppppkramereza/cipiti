<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Coming Soon | Cipiti</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assetscp/images/cipiti1.png') }}">

    <!-- App css -->
    <link href="assetscp/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="assetscp/css/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <!-- App-dark css -->
    <link href="assetscp/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet"
        disabled="disabled" />
    <link href="assetscp/css/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet"
        disabled="disabled" />

    <!-- icons -->
    <link href="assetscp/css/icons.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="loading authentication-bg">

    <div class="mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <div class="text-center">
                        <div class="login-logo">
                            <img src="{{ asset('assetscp/images/cipitilg.png') }}" alt="logo" width="180">
                        </div>

                        <h3 class="mt-4" style="color: white">Stay tunned, we're launching very soon</h3>
                        <p class="text-muted" style="color: white;font-size: 18px">We're making the system more
                            awesome.</p>
                        <p class="text-muted" style="color: red;font-size: 18px"><a
                                href="https://bit.ly/Cipiti-apps">"Try Our
                                Apps"</a></p>

                    </div>
                </div>
            </div>
            {{-- <div class="row mt-5 justify-content-center">
                    <div class="col-md-8 text-center">
                        <div data-countdown="2022/04/01" class="counter-number"></div>
                    </div> <!-- end col-->
                </div> <!-- end row--> --}}

        </div>
    </div>

    <!-- Vendor -->
    <script src="assetscp/libs/jquery/jquery.min.js"></script>
    <script src="assetscp/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assetscp/libs/simplebar/simplebar.min.js"></script>
    <script src="assetscp/libs/node-waves/waves.min.js"></script>
    <script src="assetscp/libs/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="assetscp/libs/jquery.counterup/jquery.counterup.min.js"></script>
    <script src="assetscp/libs/feather-icons/feather.min.js"></script>

    <!-- Plugins js-->
    <script src="assetscp/libs/jquery-countdown/jquery.countdown.min.js"></script>

    <!-- Countdown js -->
    <script src="assetscp/js/pages/coming-soon.init.js"></script>

    <!-- App js -->
    <script src="assetscp/js/app.min.js"></script>

</body>

</html>
