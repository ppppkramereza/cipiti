<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Cipiti</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assetslp/images/cipiti1.png">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="assetslp/css/bootstrap.min.css" type="text/css">

        <!--Material Icon -->
        <link rel="stylesheet" type="text/css" href="assetslp/css/materialdesignicons.min.css" />

        <!--pe-7 Icon -->
        <link rel="stylesheet" type="text/css" href="assetslp/css/pe-icon-7-stroke.css" />

        <!-- Custom  sCss -->
        <link rel="stylesheet" type="text/css" href="assetslp/css/style.css" />

    </head>

    <body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="58" class="scrollspy-example">

      @yield('content')
        <!-- javascript -->
        <script src="assetslp/js/bootstrap.bundle.min.js"></script>
        <!-- counter js -->
        <script src="assetslp/js/counter.int.js"></script>
        <!-- custom js -->
        <script src="assetslp/js/app.js"></script>
    </body>

</html>
