<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Cipiti - Email Verification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body style="margin: 0; padding: 0; box-sizing: border-box;">
    <table align="center" cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td align="center">
                <table align="center" cellpadding="0" cellspacing="0" width="600" style="border-spacing: 2px 5px;"
                    bgcolor="#fff">
                    <tr>
                        <td align="center" style="padding: 5px 5px 5px 5px;">
                            <a href="https://cipiti.co" target="_blank">
                                <img src="{{ asset('assetscp/images/cipiti1.png') }}" alt="Logo"
                                    style="width:420px; margin: -100px -100px; border:0;" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff">
                            <table cellpadding="0" cellspacing="0" width="100%%">
                                <tr>
                                    <td
                                        style="padding: 10px 0 10px 0; font-family: Nunito, sans-serif; font-size: 20px; font-weight: 900">
                                        {{ $details['body'] }}
                                        <br>
                                        Aktifkan Akun Cipiti Anda!
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td bgcolor="#fff">
                            <table cellpadding="0" cellspacing="0" width="100%%">
                                <tr>
                                    <td
                                        style="padding: 20px 0 20px 0; font-family: Nunito, sans-serif; font-size: 14px;">
                                        Hi, <span id="name">{{ $details['body'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0; font-family: Nunito, sans-serif; font-size: 14px;">
                                        Terima kasih telah mendaftar di Cipiti. Harap konfirmasi email ini untuk
                                        mengaktifkan akun Bantuan Anda.
                                    </td>
                                </tr>
                                <tr>
                                    <td
                                        style="padding: 20px 0 20px 0; font-family: Nunito, sans-serif; font-size: 14px; text-align: center;">
                                        <a href="{{ route('verif', ['email' => $details['email']]) }}" type="button"
                                            style="background-color: #e04420; border: none; color: white; padding: 15px 40px; text-align: center; display: inline-block; font-family: Nunito, sans-serif; font-size: 18px; font-weight: bold; cursor: pointer;">
                                            Konfimasi Email
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0; font-family: Nunito, sans-serif; font-size: 14px;">
                                        Jika Anda mengalami masalah saat mengeklik tombol "Konfirmasi Email", salin dan
                                        tempel URL di bawah ini ke browser Anda:
                                        <p id="url"></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0; font-family: Nunito, sans-serif; font-size: 14px;">
                                        Password : {{ $details['pass'] }}

                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 50px 0; font-family: Nunito, sans-serif; font-size: 14px;">
                                        Regards, Cipiti.co
                                        <p>Help</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</body>

</html>
