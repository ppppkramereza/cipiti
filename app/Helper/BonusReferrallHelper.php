<?php

namespace App\Helper\BonusReferrallHelper;


use App\Models\Wallet;
use App\Models\Wallethistory;
use App\Models\User;
use App\Models\Upline;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

function bonussendwallet($id, $nominal, $user)
{

    logger(array('user = ' . $user->id, 'nominal = ' . $nominal, 'id = ' . $id));
    $balance = Wallet::where('user_id', $id->upline_id)->first();
    Wallet::where('user_id', $id->upline_id)->update([
        'balance' => $balance->balance + $nominal,
    ]);
    Wallethistory::create([
        'user_id' => $id->upline_id,
        'wallet_id' => $balance->id,
        'hystory_type_id' => 1,
        'time' => Carbon::now(),
        'note' => 'Bonus Profit sharing dari' . $user->id . ' : ' . $user->name,
        'amount' => $nominal,
        'token' => Str::random(60),
        'status' => 2,
    ]);
}

function bonuscek($idUser, $LV, $total)
{
    //id usernya
    $id = $idUser;
    //level dari user
    $fromLv = $LV;
    //total amount
    $amount = $total;
    do {
        $returnReq = bonusuplinechecker($id, $amount, $fromLv, $total);
        logger(array('==============', $returnReq));
        $id = $returnReq[0];
        $amount = $returnReq[1];
        $fromLv = $returnReq[2];
    } while ($amount > 0 && $fromLv < 7);
}

function bonusuplinechecker($id, $amount1, $Lv, $total)
{
    $amount = $amount1;
    // $fromLV = $Lv;
    $user = User::find($id);

    $uplineRaw = Upline::with(['user', 'upline'])->where('user_id', $id)->get();
    logger($uplineRaw);
    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();

    $ids = $upline->upline->id;
    $lvl = $upline->upline->level_id;


    logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));

    if ($lvl == 7 && $Lv < 7) {
        logger('masuk lvl 7');
        bonussendwallet($upline, $amount, $user);

        $amount = 0;
        return array($upline->upline_id, $amount, 7); //array 1= id upline, 2= sisa saldo yg dibagi, 3= lvl sekarang
    }
    if ($lvl == 6 && $Lv < 6) {
        logger('masuk lvl 6');
        $amount = bonusuplinechecker6($Lv, $amount, $upline,  $user,  $total);
        return array($upline->upline_id, $amount, 6);
    }
    if ($lvl == 5 && $Lv < 5) {
        $amount = bonusuplinechecker5($Lv, $amount, $upline,  $user,  $total);
        return array($upline->upline_id, $amount, 5);
    }
    if ($lvl == 4 && $Lv < 4) {
        $amount = bonusuplinechecker4($Lv, $amount, $upline, $user,  $total);
        return array($upline->upline_id, $amount, 4);
    }
    if ($lvl == 3 && $Lv < 3) {
        $amount = bonusuplinechecker3($Lv, $amount, $upline,  $user,  $total);
        return array($upline->upline_id, $amount, 3);
    }
    // if ($lvl == 2 && $Lv < 2) {
    //     $amount =bonus uplinechecker2($Lv, $amount, $upline,  $user);
    //     return array($upline->upline_id, $amount, 2);
    // }
    // if ($lvl == 1 && $Lv < 1) {
    //     $amount = uplinechecker1($Lv, $amount, $upline, $user);
    //     return array($upline->upline_id, $amount, 1);
    // }
    return array($upline->upline_id, $amount, $Lv);
}


function bonusuplinechecker6($fromLv, $amount, $upline, $user, $total)
{
    switch ($fromLv) {
        case 1:
            $nominal = $total * 0.4;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 2:
            $nominal = $total * 0.4;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 3:
            $nominal = $total * 0.3;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 4:
            $nominal = $total * 0.2;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 5:
            $nominal = $total * 0.1;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        default:
            $nominal = $total * 0.5;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
    }
}
function bonusuplinechecker5($fromLv, $amount, $upline, $user, $total)
{
    switch ($fromLv) {
        case 1:
            $nominal = $total * 0.3;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 2:
            $nominal = $total * 0.3;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 3:
            $nominal = $total * 0.2;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 4:
            $nominal = $total * 0.1;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        default:
            $nominal = $total * 0.4;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
    }
}
function bonusuplinechecker4($fromLv, $amount, $upline, $user, $total)
{
    switch ($fromLv) {
        case 1:
            $nominal = $total * 0.2;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 2:
            $nominal = $total * 0.2;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        case 3:
            $nominal = $total * 0.1;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
            break;
        default:
            $nominal = $total * 0.3;
            bonussendwallet($upline, $nominal, $user);
            return $amount - $nominal;
    }
}
function bonusuplinechecker3($fromLv, $amount, $upline, $user, $total)
{
    $nominal = $total * 0.2;
    bonussendwallet($upline, $nominal, $user);
    return $amount - $nominal;
}


function coba()
{
    dd('coba');
}
