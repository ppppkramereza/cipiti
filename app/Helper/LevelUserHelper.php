<?php

namespace App\Helper;

use App\Models\Activationaccount;
use App\Models\Downline;
use App\Models\Levels;
use App\Models\Totalbawah;
use App\Models\Wallet;
use App\Models\Wallethistory;
use App\Models\User;
use App\Models\Upline;
use App\Models\Walletmt5;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;



function levelceklevel($id)
{
    logger('ceklevel ' . $id);

    $level = Levels::get();
    $datauser = User::where('id', $id)->first();
    $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();

    $return = [];

    foreach ($data as $key => $value) {
        $a = $value->downline->id;
        $b = Activationaccount::where('user_id', $a)->where('status', 5)->first();
        if ($b != NULL) {
            array_push($return, $value);
        }
    }
    $total_direct = count($return);
    $total_akun = Totalbawah::where('user_up', $id)->count();

    $lvlnow = 6;

    for ($i = count($level) - 2; $i > 0; $i--) {
        if (
            $level[$i]->bot_request > $total_akun ||
            $level[$i]->sponsor_request > $total_direct
        ) {
            $lvlnow  = $i;
        }
    }

    if ($datauser->level != $lvlnow) {
        User::where('id', $id)->update([
            'level_id' => $lvlnow,
        ]);
    }
}

function levelcek($idUser)
{
    logger('inicek');
    logger('cek ' . $idUser);
    $id = $idUser;
    do {
        levelceklevel($id);
        $returnReq = leveluplinechecker($id);
        logger('++++++++' . $returnReq[0]);
        $id = $returnReq[0];
    } while ($id != 1);
}


function leveluplinechecker($id)
{
    logger('uplinechecker ' . $id);

    $user = User::find($id);

    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();

    return array($upline->upline_id);
}
