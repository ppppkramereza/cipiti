<?php

namespace App\Helper\ReferralHelper;

use App\Models\Totalbawah;
use App\Models\Wallet;
use App\Models\Wallethistory;
use App\Models\User;
use App\Models\Upline;
use App\Models\UserBuy;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;



function sendwallet($id, $nominal, $user, $idasli)
{

    logger(array('user = ' . $user->id, 'nominal = ' . $nominal, 'id = ' . $id));
    $balance = Wallet::where('user_id', $id->upline_id)->first();
    $userasli = User::where('id', $idasli)->first();
    Wallet::where('user_id', $id->upline_id)->update([
        'balance' => $balance->balance + $nominal,
    ]);
    Wallethistory::create([
        'user_id' => $id->upline_id,
        'wallet_id' => $balance->id,
        'hystory_type_id' => 1,
        'time' => Carbon::now(),
        'note' => 'New Member  : ' . $userasli->name,
        'amount' => $nominal,
        'token' => Str::random(60),
        'status' => 2,
    ]);
}

function plusrobot($id)
{
    $user = User::where('id', $id)->first();
    $tambah = $user->total_robot + 1;
    User::where('id', $id)->update([
        'total_robot' => $tambah,
    ]);
    UserBuy::create([
        'user_id' => $id,
        'user_id_buy' => $id
    ]);
}

function minusrobot($id)
{
    $user = User::where('id', $id)->first();
    $tambah = $user->total_robot - 1;
    User::where('id', $id)->update([
        'total_robot' => $tambah,
    ]);
}


function cek($idUser)
{
    $id = $idUser;
    $idasli = $idUser;
    $fromLv = 0;
    $amount = 100;
    do {
        $returnReq = uplinechecker($id, $amount, $fromLv, $idasli);
        logger(array('==============', $returnReq));
        $id = $returnReq[0];
        $amount = $returnReq[1];
        $fromLv = $returnReq[2];
    } while ($amount > 0 && $fromLv < 7);
}


function uplinechecker($id, $amount1, $Lv, $idasli)
{
    $amount = $amount1;
    // $fromLV = $Lv;
    $user = User::find($id);

    $uplineRaw = Upline::with(['user', 'upline'])->where('user_id', $id)->get();
    logger($uplineRaw);
    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();

    if ($upline->upline->id == 1) {
        $ids = 1;
        $lvl = 7;
        // dd($upline);
        // plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    } else {
        $ids = $upline->upline->id;
        $lvl = $upline->upline->level_id;
        // dd($upline);
        // plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    }

    if ($lvl == 7 && $Lv < 7) {
        logger('masuk lvl 7');
        sendwallet($upline, $amount, $user, $idasli);

        $amount = 0;
        return array($upline->upline_id, $amount, 7); //array 1= id upline, 2= sisa saldo yg dibagi, 3= lvl sekarang
    }
    if ($lvl == 6 && $Lv < 6) {
        logger('masuk lvl 6');
        $amount = uplinechecker6($Lv, $amount, $upline,  $user, $idasli);
        return array($upline->upline_id, $amount, 6);
    }
    if ($lvl == 5 && $Lv < 5) {
        $amount = uplinechecker5($Lv, $amount, $upline,  $user, $idasli);
        return array($upline->upline_id, $amount, 5);
    }
    if ($lvl == 4 && $Lv < 4) {
        $amount = uplinechecker4($Lv, $amount, $upline, $user, $idasli);
        return array($upline->upline_id, $amount, 4);
    }
    if ($lvl == 3 && $Lv < 3) {
        $amount = uplinechecker3($Lv, $amount, $upline,  $user, $idasli);
        return array($upline->upline_id, $amount, 3);
    }
    if ($lvl == 2 && $Lv < 2) {
        $amount = uplinechecker2($Lv, $amount, $upline,  $user, $idasli);
        return array($upline->upline_id, $amount, 2);
    }
    if ($lvl == 1 && $Lv < 1) {
        $amount = uplinechecker1($Lv, $amount, $upline, $user, $idasli);
        return array($upline->upline_id, $amount, 1);
    }
    return array($upline->upline_id, $amount, $Lv);
}

function uplinechecker6($fromLv, $amount, $upline, $user, $idasli)
{
    switch ($fromLv) {
        case 1:
            sendwallet($upline, 40, $user, $idasli);

            return $amount - 40;
            break;
        case 2:
            sendwallet($upline, 30, $user, $idasli);

            return $amount - 30;
            break;
        case 3:
            sendwallet($upline, 20, $user, $idasli);

            return $amount - 20;
            break;
        case 4:
            sendwallet($upline, 10, $user, $idasli);

            return $amount - 10;
            break;
        case 5:
            sendwallet($upline, 5, $user, $idasli);

            return $amount - 5;
            break;
        default:
            sendwallet($upline, 55, $user, $idasli);

            return $amount - 55;
    }
}
function uplinechecker5($fromLv, $amount, $upline, $user, $idasli)
{
    switch ($fromLv) {
        case 1:
            sendwallet($upline, 35, $user, $idasli);

            return $amount - 35;
            break;
        case 2:
            sendwallet($upline, 25, $user, $idasli);

            return $amount - 25;
            break;
        case 3:
            sendwallet($upline, 15, $user, $idasli);

            return $amount - 15;
            break;
        case 4:
            sendwallet($upline, 5, $user, $idasli);

            return $amount - 5;
            break;
        default:
            sendwallet($upline, 50, $user, $idasli);

            return $amount - 50;
    }
}
function uplinechecker4($fromLv, $amount, $upline, $user, $idasli)
{
    switch ($fromLv) {
        case 1:
            sendwallet($upline, 30, $user, $idasli);

            return $amount - 30;
            break;
        case 2:
            sendwallet($upline, 20, $user, $idasli);

            return $amount - 20;
            break;
        case 3:
            sendwallet($upline, 10, $user, $idasli);

            return $amount - 10;
            break;
        default:
            sendwallet($upline, 45, $user, $idasli);

            return $amount - 45;
    }
}
function uplinechecker3($fromLv, $amount, $upline, $user, $idasli)
{
    // logger(array($fromLv, $amount, $upline, $user));
    switch ($fromLv) {
        case 1:
            sendwallet($upline, 20, $user, $idasli);

            return $amount - 20;
            break;
        case 2:
            sendwallet($upline, 10, $user, $idasli);

            return $amount - 10;
            break;
        default:
            sendwallet($upline, 35, $user, $idasli);

            return $amount - 35;
    }
}
function uplinechecker2($fromLv, $amount, $upline, $user, $idasli)
{
    switch ($fromLv) {
        case 1:
            sendwallet($upline, 10, $user, $idasli);

            return $amount - 10;
            break;
        default:
            sendwallet($upline, 25, $user, $idasli);

            return $amount - 25;
    }
}
function uplinechecker1($fromLv, $amount, $upline, $user, $idasli)
{
    sendwallet($upline, 15, $user, $idasli);

    return $amount - 15;
}

//robot minus
function cekminusrobot($idUser)
{
    $id = $idUser;
    $fromLv = 0;
    $amount = 100;
    do {
        $returnReq = uplinecheckerminusrobot($id, $amount, $fromLv);
        logger(array('==============', $returnReq));
        $id = $returnReq[0];
        $amount = $returnReq[1];
        $fromLv = $returnReq[2];
    } while ($amount > 0 && $fromLv < 7);
}

function uplinecheckerminusrobot($id, $amount1, $Lv)
{
    $amount = $amount1;
    // $fromLV = $Lv;
    $user = User::find($id);

    $uplineRaw = Upline::with(['user', 'upline'])->where('user_id', $id)->get();
    logger($uplineRaw);
    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();

    if ($upline->upline->id == 1) {
        $ids = 1;
        $lvl = 7;
        // dd($upline);
        minusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    } else {
        $ids = $upline->upline->id;
        $lvl = $upline->upline->level_id;
        // dd($upline);
        minusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    }
    if ($lvl == 7 && $Lv < 7) {
        logger('masuk lvl 7');
        // sendwallet($upline, $amount, $user);

        $amount = 0;
        return array($upline->upline_id, $amount, 7); //array 1= id upline, 2= sisa saldo yg dibagi, 3= lvl sekarang
    }
    if ($lvl == 6 && $Lv < 6) {
        logger('masuk lvl 6');
        $amount = uplinechecker6minusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 6);
    }
    if ($lvl == 5 && $Lv < 5) {
        $amount = uplinechecker5minusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 5);
    }
    if ($lvl == 4 && $Lv < 4) {
        $amount = uplinechecker4minusrobot($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 4);
    }
    if ($lvl == 3 && $Lv < 3) {
        $amount = uplinechecker3minusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 3);
    }
    if ($lvl == 2 && $Lv < 2) {
        $amount = uplinechecker2minusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 2);
    }
    if ($lvl == 1 && $Lv < 1) {
        $amount = uplinechecker1minusrobot($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 1);
    }
    return array($upline->upline_id, $amount, $Lv);
}

function uplinechecker6minusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
            break;
        case 2:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 3:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 4:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        case 5:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 60, $user);

            return $amount - 60;
    }
}
function uplinechecker5minusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 35, $user);

            return $amount - 35;
            break;
        case 2:
            // sendwallet($upline, 25, $user);

            return $amount - 25;
            break;
        case 3:
            // sendwallet($upline, 15, $user);

            return $amount - 15;
            break;
        case 4:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 55, $user);

            return $amount - 55;
    }
}
function uplinechecker4minusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 2:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 3:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 50, $user);

            return $amount - 50;
    }
}
function uplinechecker3minusrobot($fromLv, $amount, $upline, $user)
{
    // logger(array($fromLv, $amount, $upline, $user));
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 2:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
    }
}
function uplinechecker2minusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
    }
}
function uplinechecker1minusrobot($fromLv, $amount, $upline, $user)
{
    // sendwallet($upline, 20, $user);

    return $amount - 20;
}

//robot plus
function cekplusrobot($idUser)
{
    $id = $idUser;
    $fromLv = 0;
    $amount = 100;
    do {
        $returnReq = uplinecheckerplusrobot($id, $amount, $fromLv);
        logger(array('==============', $returnReq));
        $id = $returnReq[0];
        $amount = $returnReq[1];
        $fromLv = $returnReq[2];
    } while ($amount > 0 && $fromLv < 7);
}

function uplinecheckerplusrobot($id, $amount1, $Lv)
{
    $amount = $amount1;
    // $fromLV = $Lv;
    $user = User::find($id);

    $uplineRaw = Upline::with(['user', 'upline'])->where('user_id', $id)->get();
    logger($uplineRaw);
    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();

    if ($upline->upline->id == 1) {
        $ids = 1;
        $lvl = 7;
        // dd($upline);
        plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    } else {
        $ids = $upline->upline->id;
        $lvl = $upline->upline->level_id;
        // dd($upline);
        plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    }
    if ($lvl == 7 && $Lv < 7) {
        logger('masuk lvl 7');
        // sendwallet($upline, $amount, $user);

        $amount = 0;
        return array($upline->upline_id, $amount, 7); //array 1= id upline, 2= sisa saldo yg dibagi, 3= lvl sekarang
    }
    if ($lvl == 6 && $Lv < 6) {
        logger('masuk lvl 6');
        $amount = uplinechecker6plusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 6);
    }
    if ($lvl == 5 && $Lv < 5) {
        $amount = uplinechecker5plusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 5);
    }
    if ($lvl == 4 && $Lv < 4) {
        $amount = uplinechecker4plusrobot($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 4);
    }
    if ($lvl == 3 && $Lv < 3) {
        $amount = uplinechecker3plusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 3);
    }
    if ($lvl == 2 && $Lv < 2) {
        $amount = uplinechecker2plusrobot($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 2);
    }
    if ($lvl == 1 && $Lv < 1) {
        $amount = uplinechecker1plusrobot($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 1);
    }
    return array($upline->upline_id, $amount, $Lv);
}

function uplinechecker6plusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
            break;
        case 2:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 3:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 4:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        case 5:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 60, $user);

            return $amount - 60;
    }
}
function uplinechecker5plusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 35, $user);

            return $amount - 35;
            break;
        case 2:
            // sendwallet($upline, 25, $user);

            return $amount - 25;
            break;
        case 3:
            // sendwallet($upline, 15, $user);

            return $amount - 15;
            break;
        case 4:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 55, $user);

            return $amount - 55;
    }
}
function uplinechecker4plusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 2:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 3:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 50, $user);

            return $amount - 50;
    }
}
function uplinechecker3plusrobot($fromLv, $amount, $upline, $user)
{
    // logger(array($fromLv, $amount, $upline, $user));
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 2:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
    }
}
function uplinechecker2plusrobot($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
    }
}
function uplinechecker1plusrobot($fromLv, $amount, $upline, $user)
{
    // sendwallet($upline, 20, $user);

    return $amount - 20;
}

function inserttotalbawah($idup, $idawal)
{
    Totalbawah::create(
        [
            'user_up' => $idup,
            'user_baru' => $idawal,
        ]
    );
}

function cektotalbawah($idUser)
{
    $id = $idUser;
    $idawal = $idUser;
    $fromLv = 0;
    $amount = 100;
    do {
        $returnReq = uplinecheckertotalbawah($id, $amount, $fromLv, $idawal);
        logger(array('==============', $returnReq));
        $id = $returnReq[0];
        $amount = $returnReq[1];
        $fromLv = $returnReq[2];
    } while ($amount > 0 && $fromLv < 7);
}

function uplinecheckertotalbawah($id, $amount1, $Lv, $idawal)
{
    $amount = $amount1;
    // $fromLV = $Lv;
    $user = User::find($id);

    $uplineRaw = Upline::with(['user', 'upline'])->where('user_id', $id)->get();
    logger($uplineRaw);
    $upline = Upline::with(['user', 'upline'])->where('user_id', $id)->first();


    if ($upline->upline->id == 1) {
        $ids = 1;
        $lvl = 7;
        // dd($upline);
        // plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    } else {
        $ids = $upline->upline->id;
        $lvl = $upline->upline->level_id;
        // dd($upline);
        // plusrobot($ids);
        logger(array('=======array=======', 'id sekarang = ' . $user->id, 'id upline = ' . $ids, 'lvl = ' . $lvl));
    }

    inserttotalbawah($ids, $idawal);
    if ($lvl == 7 && $Lv < 7) {
        logger('masuk lvl 7');
        // sendwallet($upline, $amount, $user);

        $amount = 0;
        return array($upline->upline_id, $amount, 7); //array 1= id upline, 2= sisa saldo yg dibagi, 3= lvl sekarang
    }
    if ($lvl == 6 && $Lv < 6) {
        logger('masuk lvl 6');
        $amount = uplinechecker6totalbawah($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 6);
    }
    if ($lvl == 5 && $Lv < 5) {
        $amount = uplinechecker5totalbawah($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 5);
    }
    if ($lvl == 4 && $Lv < 4) {
        $amount = uplinechecker4totalbawah($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 4);
    }
    if ($lvl == 3 && $Lv < 3) {
        $amount = uplinechecker3totalbawah($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 3);
    }
    if ($lvl == 2 && $Lv < 2) {
        $amount = uplinechecker2totalbawah($Lv, $amount, $upline,  $user);
        return array($upline->upline_id, $amount, 2);
    }
    if ($lvl == 1 && $Lv < 1) {
        $amount = uplinechecker1totalbawah($Lv, $amount, $upline, $user);
        return array($upline->upline_id, $amount, 1);
    }
    return array($upline->upline_id, $amount, $Lv);
}

function uplinechecker6totalbawah($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
            break;
        case 2:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 3:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 4:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        case 5:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 60, $user);

            return $amount - 60;
    }
}
function uplinechecker5totalbawah($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 35, $user);

            return $amount - 35;
            break;
        case 2:
            // sendwallet($upline, 25, $user);

            return $amount - 25;
            break;
        case 3:
            // sendwallet($upline, 15, $user);

            return $amount - 15;
            break;
        case 4:
            // sendwallet($upline, 5, $user);

            return $amount - 5;
            break;
        default:
            // sendwallet($upline, 55, $user);

            return $amount - 55;
    }
}
function uplinechecker4totalbawah($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
            break;
        case 2:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 3:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 50, $user);

            return $amount - 50;
    }
}
function uplinechecker3totalbawah($fromLv, $amount, $upline, $user)
{
    // logger(array($fromLv, $amount, $upline, $user));
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 20, $user);

            return $amount - 20;
            break;
        case 2:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 40, $user);

            return $amount - 40;
    }
}
function uplinechecker2totalbawah($fromLv, $amount, $upline, $user)
{
    switch ($fromLv) {
        case 1:
            // sendwallet($upline, 10, $user);

            return $amount - 10;
            break;
        default:
            // sendwallet($upline, 30, $user);

            return $amount - 30;
    }
}
function uplinechecker1totalbawah($fromLv, $amount, $upline, $user)
{
    // sendwallet($upline, 20, $user);

    return $amount - 20;
}



function coba()
{
    dd('coba');
}
