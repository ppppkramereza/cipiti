<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\Activationaccount;
use App\Models\Downline;
use App\Models\Upline;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Walletmt5;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Session;

use function App\Helper\ReferralHelper\cekminusrobot;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->input('email');
        $phone = $request->input('phone');


        $cek = User::where('email', $email)->orWhere('phone', $phone)->first();
        if ($cek) {
            return redirect()->route('dashboard.dev.member.index')->with('gagal', '.');
        }

        $cekrefferal = User::where('refferal', $request->input('refferal'))->first();
        if (!$cekrefferal) {
            return redirect()->route('dashboard.dev.member.index')->with('gagalrefferal', '.');
        }

        $data = user::create([
            'name' => $request->input('nama'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'country_code' => $request->input('country_code'),
            'phone' => $request->input('phone'),
            'refferal' => Str::random(8) . Carbon::now() . Str::random(8),
            'status' => 1,
            'level_id' => 1,
            'total_robot' => 0,
            'created_at' => Carbon::now()
        ]);

        $cekid = User::where('email', $email)->where('phone', $phone)->first();
        upline::create([
            'upline_id' => $cekrefferal->id,
            'user_id' => $cekid->id,
            'created_at' => Carbon::now()
        ]);
        downline::create([
            'downline_id' => $cekid->id,
            'user_id' => $cekrefferal->id,
            'created_at' => Carbon::now()
        ]);

        $save = DB::table('model_has_roles')->insert([
            'role_id' => 7,
            'model_type' => "App\Models\User",
            'model_id' => $cekid->id
        ]);
        return redirect()->route('dashboard.dev.member.index')->with('berhasil', '.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if ($request->input('mode') == 'profil') {
            User::where('id', $request->id)->update([
                'name' => $request->input('nama'),
                'email' => $request->input('email'),
                'country_code' => $request->input('country_code'),
                'phone' => $request->input('phone'),
            ]);
        }
        if ($request->input('mode') == 'password') {
            User::where('id', $request->id)->update([
                'password' => Hash::make($request->input('password')),
            ]);
        }
        if ($request->input('mode') == 'upline') {

            upline::where('user_id', $request->id)->update([
                'upline_id' => $request->input('upline'),
            ]);

            downline::where('downline_id', $request->id)->update([
                'user_id' => $request->input('upline'),
            ]);
        }

        return redirect()->route('dashboard.dev.member.index')->with('berhasilupdate', '.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        // dd($data);
        if ($data->status == 3) {
            cekminusrobot($id);
        }
        Downline::where('downline_id', $id)->delete();
        Upline::where('user_id', $id)->delete();
        Wallet::where('user_id', $id)->delete();
        Walletmt5::where('user_id', $id)->delete();
        Activationaccount::where('user_id', $id)->delete();
        $data->delete();


        return redirect()->route('dashboard.dev.member.index')->with('hapus', '.');
    }

    public function index()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        //$datas = User::Role('Customer')->get();
        //$datas = User::all();
        $datas = User::with(['roles', 'leveluser', 'statususer', 'wallet'])->Role('User')->orderBy('created_at', 'DESC')->get();

        Session::put('menu', 'manajemenuser');
        Session::put('sub', 'userapps');
        return view('page.dashboard.dev.member.index', compact(['datas']));
        //return ($datas);
    }
}
