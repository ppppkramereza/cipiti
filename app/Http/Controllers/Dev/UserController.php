<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\Activationaccount;
use App\Models\Downline;
use App\Models\Upline;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Walletmt5;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Session;

use function App\Helper\ReferralHelper\cekminusrobot;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        $datas = User::with(['roles', 'mt5'])->Role(['Super Admin', 'Admin', 'Owner', 'User', 'Leader'])->orderBy('created_at', 'DESC')->get();

        //dd($datas)
        // $datas = DB::table('users')
        //    ->select('users.*','status.name as nama_status','roles.name as nama_roles')
        //    ->join('status','status.id','=','users.status')
        //    ->join('model_has_roles','model_has_roles.model_id','=','users.id')
        //    ->join('roles','model_has_roles.role_id','=','roles.id')
        //    ->where('model_has_roles.role_id', 'NOT LIKE' ,7)
        //    ->where('users.deleted_at', '=',null)
        //    ->get();

        //$datas = User::all();
        return view('page.dashboard.dev.user.index', compact(['datas']));
        //return ($datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.dashboard.dev.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request->input('email');

        $cek = User::where('email', $email)->first();
        if ($cek) {
            return redirect()->route('dashboard.dev.user.index')->with('gagal', '.');
        }

        $data = user::create([
            'name' => $request->input('nama'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'country_code' => '+62',
            'phone' => Str::random(4),
            'refferal' => Str::random(4),
            'status' => 1,
            'level_id' => 1,
        ]);


        $user = User::where('email', $email)->first();

        $save = DB::table('model_has_roles')->insert([
            'role_id' => $request->input('role'),
            'model_type' => "App\Models\User",
            'model_id' => $user->id
        ]);
        return redirect()->route('dashboard.dev.user.index')->with('berhasil', '.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        User::where('id', $request->id)->update([
            'name' => $request->nama,
            'password' => Hash::make($request->input('password')),
            'email' => $request->email
        ]);

        DB::table('model_has_roles')->where('model_id', $request->id)->update([
            'role_id' => $request->role,
        ]);

        return redirect()->route('dashboard.dev.user.index')->with('berhasilupdate', '.');
    }

    public function password(Request $request)
    {

        $user = User::find(auth()->user()->id);

        if (!Hash::check($request->input('passwordlama'), $user->password)) {
            return redirect()->back()->with('updatepasswordgagal', '.');
        }

        User::where('id', $request->input('id'))->update([
            'password' => Hash::make($request->input('passwordbaru'))
        ]);

        auth()->user()->password = Hash::make($request->input('passwordbaru'));

        return redirect()->back()->with('updatepassword', '.');
    }


    public function profil(Request $request)
    {
        User::where('id', $request->input('id'))->update([
            'name' => $request->input('nama'),
            'email' => $request->input('email')
        ]);

        auth()->user()->email = $request->input('email');
        auth()->user()->name = $request->input('nama');


        return redirect()->back()->with('updateprofil', '.');
    }

    public function storemt5(Request $request)
    {
        $username = $request->input('username');

        $cek = DB::select("select * from account_mt5 where username='$username'");
        if (count($cek) > 0) {
            return redirect()->route('dashboard.dev.member.index')->with('gagalmt5', '.');
        }

        $save = DB::table('account_mt5')->insert([
            'user_id' => $request->input('id'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'server' => $request->input('server'),
            'ip_server' => $request->input('server_ip'),
        ]);
        return redirect()->route('dashboard.dev.member.index')->with('berhasil', '.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        Downline::where('downline_id', $id)->delete();
        Upline::where('user_id', $id)->delete();
        Wallet::where('user_id', $id)->delete();
        Walletmt5::where('user_id', $id)->delete();
        Activationaccount::where('user_id', $id)->delete();
        $data->delete();

        // cekminusrobot($id);

        return redirect()->route('dashboard.dev.user.index')->with('hapus', '.');
    }

    public function indexmember()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        //$datas = User::Role('Customer')->get();
        //$datas = User::all();
        $datas = DB::table('users')
            ->select('users.*', 'status.name as nama_status', 'roles.name as nama_roles', 'levels.name as nama_level')
            ->join('status', 'status.id', '=', 'users.status')
            ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('levels', 'levels.id', '=', 'users.level_id')
            ->where('model_has_roles.role_id', '=', 7)
            ->where('users.deleted_at', '=', null)
            ->get();
        return view('page.dashboard.dev.user.indexmember', compact(['datas']));
    }
}
