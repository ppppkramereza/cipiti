<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Bankuser;
use App\Models\Bankkode;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
class MemberBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
        $datas = Bankuser::with(['user','bank'])
            ->orderBy('created_at', 'DESC')
            ->get();

            $data = DB::select("select * from users");
            $datab  = DB::select("select * from bank_kodes");

        Session::put('menu','masterdata');
        Session::put('sub','memberbank');
        // $datas = DB::select("select * from bank_users");
        return view('page.dashboard.dev.member-bank.index', compact(['datas','data','datab']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = Bankuser::create([
                'bank_id' => $request->input('bank_id'),
                'user_id' => $request->input('user_id'),
                'account_name' => $request->input('nama_akun'),
                'bank_rek' => $request->input('no_rek'),
                ]);
        return redirect()->route('dashboard.dev.member-bank.index')->with('berhasil','.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Bankuser::where('id', $request->id)->update([
                'bank_id' => $request->input('bank_id'),
                'account_name' => $request->input('nama_akun'),
                'bank_rek' => $request->input('no_rek'),
                ]);

     return redirect()->route('dashboard.dev.member-bank.index')->with('berhasilupdate','.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Bankuser::find($id);
        $data->delete();

        return redirect()->route('dashboard.dev.member-bank.index')->with('hapus', '.');
    }
}
