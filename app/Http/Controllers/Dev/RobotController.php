<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Botprice;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
class RobotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datas = mutasi_history::with('user')->get();
        // return view('page.dashboard.dev.mutasi.history', compact(['datas']));
        // $datas = DB::table('tabel_banks')->select('id','bank_name','bank_code','account_name',
        //     'account_number','created_at','updated')->get();

        $datas = Botprice::orderBy('created_at','DESC')->get();
        //$datas = User::all();

        Session::put('menu','masterdata');
        Session::put('sub','robot');
        return view('page.dashboard.dev.robot.index', compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = Botprice::create([
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'description' => $request->input('descriptionadd'),
                ]);
        return redirect()->route('dashboard.dev.robot.index')->with('berhasil','.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Botprice::where('id', $request->id)->update([
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'description' => $request->input('description'.$request->id),
                ]);

     return redirect()->route('dashboard.dev.robot.index')->with('berhasilupdate','.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Botprice::find($id);
        $data->delete();

        return redirect()->route('dashboard.dev.robot.index')->with('hapus', '.');
    }


}
