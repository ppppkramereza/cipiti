<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use App\Models\Historymoota;
use App\Models\mutasi_history;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;

class MutationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Session::put('menu', 'mutasi');
        Session::put('sub', 'check');
        return view('page.dashboard.dev.mutasi.create');
    }

    public function history()
    {
        Session::put('menu', 'mutasi');
        Session::put('sub', 'history');
        $data = Historymoota::orderBy('date', 'desc')->get();
        // $data = Webhook::get();
        $datas = json_decode($data);



        // dd(json_decode($datas[0]));
        // dd($data);
        // return json($datas);
        return view('page.dashboard.dev.mutasi.history', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        logger('asd');
        // $validated = $request->validate([
        //     'amount' => 'required|unique:posts|max:255',
        // ]);


        $response = Http::withOptions(['verify' => false])->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => config('app.token_moota'),
        ])->get('http://app.moota.co/api/v2/mutation', [
            'type' => 'CR',
            'amount' => $request->input('amount'),
            'description' => $request->input('description'),

        ]);
        $data = json_decode($response->getBody()->getContents());
        $datas = $data->data;
        // return response($datas, 200);
        return view('page.dashboard.dev.mutasi.index', compact(['datas']));
    }

    public function usercek(Request $request)
    {
        $datamoota = $request->input();
        // dd($datamoota['amount']);
        $data = User::get();
        return view('page.dashboard.dev.mutasi.detail', compact(['data', 'datamoota']));
    }

    public function store(Request $request)
    {
        mutasi_history::create([
            'user_id' => $request->input('user_id'),
            'account_number' => $request->input('account_number'),
            'date' => $request->input('date'),
            'description' => $request->input('description'),
            'amount' => $request->input('amount'),
            'type' => $request->input('type'),
            'note' => '#' . $request->input('note'),
            'balance' => $request->input('balance'),
            'created_at_moota' => $request->input('created_at'),
            'updated_at_moota' => $request->input('updated_at'),
            'status' => 'Aproved',
        ]);

        return redirect()->route('dashboard.dev.mutasi.index.history');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
