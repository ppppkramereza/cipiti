<?php

namespace App\Http\Controllers\Cashier;

use App\Http\Controllers\Controller;
use App\Models\Activationaccount;
use Illuminate\Http\Request;
use App\Jobs\ReferralJobs;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

use function App\Helper\ReferralHelper\bonuscek;
use function App\Helper\ReferralHelper\cek;
use Session;

class ActivationaccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = DB::table('activation_accounts')
            ->select('activation_accounts.*', 'status.name as nama_status', 'users.name as nama_user', 'tabel_banks.bank_name as nama_bank')
            ->join('status', 'status.id', '=', 'activation_accounts.status')
            ->join('users', 'activation_accounts.user_id', '=', 'users.id')
            ->join('tabel_banks', 'tabel_banks.id', '=', 'activation_accounts.bank_id')
            ->where('activation_accounts.status', 5)
            ->where('activation_accounts.deleted_at', null)
            ->orderBy('activation_accounts.created_at', 'DESC')
            ->get();

        Session::put('menu', 'activation');
        Session::put('sub', 'activated');
        return view('page.dashboard.cashier.activationaccount.index', compact('datas'));
    }

    public function approval()
    {
        $datas = DB::table('activation_accounts')
            ->select('activation_accounts.*', 'status.name as nama_status', 'users.name as nama_user', 'tabel_banks.bank_name as nama_bank', 'images.image_url as gambar')
            ->join('status', 'status.id', '=', 'activation_accounts.status')
            ->join('users', 'activation_accounts.user_id', '=', 'users.id')
            ->join('images', 'activation_accounts.image_id', '=', 'images.id')
            ->join('tabel_banks', 'tabel_banks.id', '=', 'activation_accounts.bank_id')
            ->where('activation_accounts.status', 4)
            ->where('activation_accounts.deleted_at', null)
            ->orderBy('activation_accounts.created_at', 'DESC')
            ->get();

        Session::put('menu', 'activation');
        Session::put('sub', 'approval');
        return view('page.dashboard.cashier.activationaccount.approval', compact('datas'));
    }

    public function approve($id)
    {

        DB::table('activation_accounts')->where('id', $id)->update([
            'status' => 5,
            'updated_at' => Carbon::now()
        ]);

        $getuserid = DB::select("select * from activation_accounts where id='$id'");
        foreach ($getuserid as $key) {
            $getrobot = DB::select("select * from users where id='$key->user_id'");
            foreach ($getrobot as $keyy) {
                DB::table('users')->where('id', $key->user_id)->update([
                    'status' => 2,
                ]);
            }
        }

        // dd($getuserid[0]->user_id);
        cek($getuserid[0]->user_id);

        return redirect()->route('dashboard.cashier.activation-account.approval')->with('berhasilapprove', '.');;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Activationaccount::where('id', $id)->update([
            'status' => 5,
        ]);

        cek($id);

        return redirect()->route('dashboard.cashier.activation-account.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
