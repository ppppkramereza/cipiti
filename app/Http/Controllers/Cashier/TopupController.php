<?php

namespace App\Http\Controllers\Cashier;

use App\Helper\LevelUserHelper;
use App\Http\Controllers\Controller;
use App\Models\Kurs;
use App\Models\Topup;
use App\Models\User;
use App\Models\Walletmt5;
use App\Models\Walletmt5history;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Session;

use function App\Helper\levelcek;

class TopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('topups')
            ->select('topups.*', 'status.name as nama_status', 'users.name as nama_user', 'tabel_banks.bank_name as nama_bank')
            ->join('status', 'status.id', '=', 'topups.status')
            ->join('users', 'topups.user_id', '=', 'users.id')
            ->join('tabel_banks', 'tabel_banks.id', '=', 'topups.bank_id')
            ->where('topups.status', 13)
            ->orWhere('topups.status', 18)
            ->orderBy('created_at', 'DESC')
            ->get();
        //$datas = User::all();
        Session::put('menu', 'topup');
        Session::put('sub', 'topupindex');
        return view('page.dashboard.cashier.topup.index', compact(['datas']));
    }

    public function approval()
    {
        $datas = DB::table('topups')
            ->select('topups.*', 'status.name as nama_status', 'users.name as nama_user', 'tabel_banks.bank_name as nama_bank')
            ->join('status', 'status.id', '=', 'topups.status')
            ->join('users', 'topups.user_id', '=', 'users.id')
            ->join('tabel_banks', 'tabel_banks.id', '=', 'topups.bank_id')
            ->where('topups.status', '=', 12)
            ->orWhere('topups.status', '=', 10)
            ->orWhere('topups.status', '=', 11)
            ->orderBy('created_at', 'DESC')
            ->get();
        //$datas = User::all();

        Session::put('menu', 'topup');
        Session::put('sub', 'topupapproval');
        return view('page.dashboard.cashier.topup.approval', compact(['datas']));
    }

    public function approve(Request $request)
    {
        $id_user = '';
        $id_wallet = '';
        $time = Carbon::now();
        $token = Str::random(8);
        $amount = 0;
        $unik = 0;
        $balance = 0;
        $usdt = 0;

        $id = $request->input('id');
        if ($request->input('tolak')) {
            DB::table('topups')->where('id', $id)->update([
                'status' => 18
            ]);

            return redirect()->route('dashboard.cashier.topup.approval')->with('berhasiltolak', '.');;
        }
        //getdata
        $getuserid = DB::select("select user_id, amount  from topups where id='$id'");
        // ceklevel();
        // levelcek($getuserid[0]->user_id);
        // HelperCeklevel()
        foreach ($getuserid as $key) {
            $id_user = $key->user_id;
            $amount = $key->amount;
            $getwalletid = DB::select("select id,balance  from walletmt5s where user_id='$id_user'");
            foreach ($getwalletid as $keyy) {
                $id_wallet = $keyy->id;
                $balance = $keyy->balance;
            }
        }
        $getusdt = DB::select("select * from kurs");
        foreach ($getusdt as $keyyy) {
            $usdt = $keyyy->harga_jual;
        }

        $topupunik = Topup::where('id', $id)->first()->unik;
        //change IDR to USDT
        $amount = round(($amount - $topupunik) / $usdt, 2);


        DB::table('topups')->where('id', $id)->update([
            'status' => 13
        ]);

        DB::table('walletmt5_historys')->insert([
            'user_id' => $id_user,
            'wallet_id' => $id_wallet,
            'hystory_type_id' => 3,
            'time' => $time,
            'note' => 'topup ' . $time,
            'amount' => $amount,
            'status' => 13,
            'token' => $token,
            'created_at' => $time
        ]);

        $modal_userold = User::where('id', $getuserid[0]->user_id)->first()->modal_user;
        User::where('id', $getuserid[0]->user_id)->update([
            'modal_user' => $modal_userold + $amount
        ]);

        DB::table('walletmt5s')->where('id', $id_wallet)->update([
            'balance' => $balance + $amount,
            'last_update' => $time,
            'last_token' => $token
        ]);        //$datas = User::all();
        return redirect()->route('dashboard.cashier.topup.approval')->with('berhasilapprove', '.');;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function indexmt5()
    {
        $selectmt5 = DB::SELECT("select * from walletmt5s a, users b where a.user_id=b.id;");

        Session::put('menu', 'topup');
        Session::put('sub', 'topupmt5');

        return view('page.dashboard.cashier.topup.topupmt5.index', compact(['selectmt5']));
    }

    public function tambah(Request $request)
    {
        $usrid = $request->id;
        $balance = $request->balance;
        $idmt5 = DB::select("select id from walletmt5s where user_id='$usrid'");
        $balanceawal = DB::select("select balance from walletmt5s where user_id='$usrid'");
        $balanceawalmodal = DB::select("select modal_user from users where id='$usrid'");
        $token = Str::random(60);        // dd($balanceawalmodal[0]->modal_user);

        $wallet = DB::table('walletmt5s')->where('user_id', $usrid)->update([
            'balance' => $balanceawal[0]->balance + (int)$balance,
            'last_update' => Carbon::now(),
            'last_token' => Str::random(60),
        ]);


        $data = Walletmt5history::create([
            'user_id' => $usrid,
            'wallet_id' => (int)$idmt5[0]->id,
            'hystory_type_id' => '4',
            'time' => Carbon::now(),
            'note' => 'Admin topupmt5',
            'amount' => $balance,
            'status' => '15',
            'token' => $token,
        ]);

        return redirect()->route('dashboard.cashier.topupmt5.index')->with('berhasil', '.');
    }

    public function tambahmodal(Request $request)
    {
        $usrid = $request->id;
        // dd($balanceawalmodal[0]->modal_user);
        $modalrp = $request->modalrupiah;
        $modaldl = $request->modal;
        $getkurs = Kurs::find(1)->harga_jual;
        $hasil = round($modalrp / $getkurs, 2);

        if ($request->modalrupiah) {
            $mu = DB::table('users')->where('id', $usrid)->update([
                'modal_user' => $hasil,
                'updated_at' => Carbon::now(),
            ]);
        } else {
            $mu = DB::table('users')->where('id', $usrid)->update([
                'modal_user' => $modaldl,
                'updated_at' => Carbon::now(),
            ]);
        }


        return redirect()->route('dashboard.cashier.topupmt5.index')->with('berhasil', '.');
    }
}
