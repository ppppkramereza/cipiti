<?php

namespace App\Http\Controllers\Cashier\masterdata;

use App\Http\Controllers\Controller;
use App\Models\Downline;
use App\Models\Upline;
use App\Models\User;
use Illuminate\Http\Request;

use function App\Helper\ReferralHelper\cekminusrobot;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = User::role('Customer')->orderby('id', 'DESC')->get();
        return view('page.dashboard.cashier.masterdata.userdata.index', compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        Downline::where('downline_id', $id)->delete();
        Upline::where('user_id', $id)->delete();
        $data->delete();

        cekminusrobot($id);

        return redirect()->route('dashboard.cashier.masterdata.user.index');
    }
}
