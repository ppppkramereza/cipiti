<?php

namespace App\Http\Controllers\Cashier;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Session;
use function App\Helper\BonusReferrallHelper\bonuscek;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = DB::table('withdraws')
            ->select('withdraws.*', 'status.name as nama_status', 'users.name as nama_user', 'bank_users.account_name as nama_akun', 'bank_kodes.nama_bank as nama_bank')
            ->join('status', 'status.id', '=', 'withdraws.status')
            ->join('users', 'withdraws.user_id', '=', 'users.id')
            ->join('bank_users', 'bank_users.id', '=', 'withdraws.bankuser_id')
            ->join('bank_kodes', 'bank_kodes.id', '=', 'bank_users.bank_id')
            ->where('withdraws.status', 15)
            ->orWhere('withdraws.status', 18)
            ->orderBy('withdraws.created_at', 'DESC')
            ->get();

        Session::put('menu','wd');
        Session::put('sub','wdindex');
        return view('page.dashboard.cashier.withdraw.index', compact(['datas']));
    }
    public function approval()
    {

        $datas = DB::table('withdraws')
            ->select('withdraws.*', 'status.name as nama_status', 'users.name as nama_user', 'bank_users.account_name as nama_akun', 'bank_kodes.nama_bank as nama_bank')
            ->join('status', 'status.id', '=', 'withdraws.status')
            ->join('users', 'withdraws.user_id', '=', 'users.id')
            ->join('bank_users', 'bank_users.id', '=', 'withdraws.bankuser_id')
            ->join('bank_kodes', 'bank_kodes.id', '=', 'bank_users.bank_id')
            ->where('withdraws.status', 14)
            ->orderBy('withdraws.created_at', 'DESC')
            ->get();

        Session::put('menu','wd');
        Session::put('sub','wdapproval');
        return view('page.dashboard.cashier.withdraw.approval', compact(['datas']));
    }


    public function approve(Request $request)
    {
        $id_user = '';
        $id_wallet = '';
        $time = Carbon::now();
        $token = Str::random(8);
        $amount = 0;
        $balance = 0;
        $usdt = 0;

        // ceklevel($id);

        $id = $request->input('id');
        
        if ($request->input('tolak')) { 
    
        DB::table('withdraws')->where('id', $id)->update([
            'status' => 18
        ]);

            return redirect()->route('dashboard.cashier.withdraw.approval')->with('berhasiltolak', '.');;
        }

        //getdata
        $getuserid = DB::select("select user_id, amount, wallet_type from withdraws where id='$id'");
        foreach ($getuserid as $key) {
            $id_user = $key->user_id;
            $amount = $key->amount;
            $type = $key->wallet_type;

            $getwalletid = DB::select("select id,balance  from walletmt5s where user_id='$id_user'");
            foreach ($getwalletid as $keyy) {
                $id_wallet = $keyy->id;
                $balance = $keyy->balance;
            }
            $getwalletsid = DB::select("select id,balance  from wallets where user_id='$id_user'");
            foreach ($getwalletsid as $keys) {
                $id_wallets = $keys->id;
                $balances = $keys->balance;
            }
        }
        $getusdt = DB::select("select * from kurs");
        foreach ($getusdt as $keyyy) {
            $usdt = $keyyy->harga_beli;
        }
        $getlvl = DB::select("select level_id from users where id='$id'");
        foreach ($getlvl as $dat) {
            $level_idd = $dat->level_id;
        }




        DB::table('withdraws')->where('id', $id)->update([
            'status' => 15
        ]);

        DB::table('walletmt5_historys')->insert([
            'user_id' => $id_user,
            'wallet_id' => $id_wallet,
            'hystory_type_id' => 4,
            'time' => $time,
            'note' => 'withdraw ' . $time,
            'amount' => $amount,
            'status' => 15,
            'token' => $token,
            'created_at' => $time
        ]);

        if ($type == '16') {
            DB::table('wallets')->where('id', $id_wallets)->update([
                'balance' => $balances - $amount,
                'last_update' => $time,
                'last_token' => $token
            ]);

            $walletwoner = Wallet::find(1)->balance;

            Wallet::where('id', 1)->update([
                'balance' => $walletwoner + ($amount * 0.10),
                'last_update' => $time,
                'last_token' => $token
            ]);
        } else {
            DB::table('walletmt5s')->where('id', $id_wallet)->update([
                'balance' => $balance - $amount,
                'last_update' => $time,
                'last_token' => $token
            ]);

            $walletwoner = Wallet::find(1)->balance;
            Wallet::where('id', 1)->update([
                'balance' => $walletwoner + ($amount * 0.10),
                'last_update' => $time,
                'last_token' => $token
            ]);
            //$datas = User::all();
        }
        // bonuscek($id_user, $level_idd, $amount * 0.20);
        return redirect()->route('dashboard.cashier.withdraw.approval')->with('berhasilapprove', '.');;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexmember()
    {
    }
}
