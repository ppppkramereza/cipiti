<?php

namespace App\Http\Controllers\Cashier\manajemenuser;

use App\Http\Controllers\Controller;
use App\Models\Accountmt5;
use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Session;

class AkunMT5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Accountmt5::with(['user'])->get();
        // dd($datas);
        // return json($datas);

        Session::put('menu','manajemenuser');
        Session::put('sub','akunmt5');
        return view('page.dashboard.cashier.manajemenuser.akunmt5.index', compact(['datas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     *

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Accountmt5::create([
            'user_id' => $request->input('user_id'),
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'password_investor' => $request->input('passwordinv'),
            'server' => $request->input('server'),
            'ip_server' => $request->input('server_ip'),
            'created_at' => Carbon::now(),
        ]);
        return redirect()->route('dashboard.cashier.manajemenuser.akunmt5.index')->with('berhasil', '.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Accountmt5::where('id', $request->input('id'))->update([
            'username' => $request->input('username'),
            'password' => $request->input('passwordedit'.$request->id),
            'password_investor' => $request->input('passwordinvedit'.$request->id),
            'server' => $request->input('server'),
            'ip_server' => $request->input('server_ip'),
            'updated_at' => Carbon::now(),
        ]);
        return redirect()->route('dashboard.cashier.manajemenuser.akunmt5.index')->with('berhasilupdate', '.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
