<?php

namespace App\Http\Controllers\Cashier\mutasi;

use App\Http\Controllers\Controller;
use App\Models\Historymoota;
use App\Models\mutasi_history;
use App\Models\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MutasiController extends Controller
{
    public function index(Request $request)
    {
        Session::put('menu', 'mutasi');
        Session::put('sub', 'check');
        return view('page.dashboard.cashier.mutasi.create');
    }

    public function history()
    {
        Session::put('menu', 'mutasi');
        Session::put('sub', 'history');
        $datas = Historymoota::orderBy('date', 'desc')->get();
        // dd($datas);
        // return json($datas);
        return view('page.dashboard.cashier.mutasi.history', compact(['datas']));
    }
    public function find(Request $request)
    {
        logger('asd');
        // $validated = $request->validate([
        //     'amount' => 'required|unique:posts|max:255',
        // ]);


        $response = Http::withOptions(['verify' => false])->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => config('app.token_moota'),
        ])->get('http://app.moota.co/api/v2/mutation', [
            'type' => 'CR',
            'amount' => $request->input('amount'),
            'description' => $request->input('description'),

        ]);
        $data = json_decode($response->getBody()->getContents());
        $datas = $data->data;
        // return response($datas, 200);
        return view('page.dashboard.cashier.mutasi.index', compact(['datas']));
    }

    public function usercek(Request $request)
    {
        $datamoota = $request->input();
        // dd($datamoota['amount']);
        $data = User::get();
        return view('page.dashboard.cashier.mutasi.detail', compact(['data', 'datamoota']));
    }

    public function store(Request $request)
    {
        mutasi_history::create([
            'user_id' => $request->input('user_id'),
            'account_number' => $request->input('account_number'),
            'date' => $request->input('date'),
            'description' => $request->input('description'),
            'amount' => $request->input('amount'),
            'type' => $request->input('type'),
            'note' => '#' . $request->input('note'),
            'balance' => $request->input('balance'),
            'created_at_moota' => $request->input('created_at'),
            'updated_at_moota' => $request->input('updated_at'),
            'status' => 'Aproved',
        ]);

        return redirect()->route('dashboard.cashier.mutasi.index.history');
    }
}
