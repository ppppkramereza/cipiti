<?php

namespace App\Http\Controllers\Customer\robot;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Activationaccount;
use App\Models\Botprice;
use App\Models\Kurs;
use App\Models\Metodepembayaran;
use App\Models\Tabelbank;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Faker\Generator as Faker;
use PDF;
use File;
use Mail;

class BeliRobotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Botprice::get();
        return view('page.dashboard.user.robot.beli-robot.index', compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
        ]);


        Activationaccount::create([
            'user_id' => '',
            'amount' => '',
            'bank_id' => '',
            'kurs_jual' => '',
            'kode_unik' => '',
            'status' => '',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Botprice::find($id);
        $kurs = Kurs::find(1);
        $metode = Metodepembayaran::get();
        $bank = Tabelbank::get();
        return view('page.dashboard.user.robot.beli-robot.show', compact(['data', 'kurs', 'metode', 'bank']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function belirobot(Request $request, Faker $faker)
    {
        User::where('id', auth()->user()->id)->update(['status' => 2]);
        $kurs = Kurs::find(1);
        $hargabot = Botprice::find(1);
        // $kodeunik = $harga - $kurs;
        $kodeunik = rand(1, 999);
        $amount = $kurs->harga_jual * $hargabot->price + $kodeunik;
        // dd();
        Activationaccount::create([
            'user_id' => auth()->user()->id,
            'amount' => $amount,
            'bank_id' => $request->input('metode_pembayaran'),
            'kurs_jual' => $kurs->harga_jual,
            'kode_unik' => $kodeunik,
            'status' => 4,
            'batas_pembayaran' => Carbon::now()->addDay(3),
        ]);
        $idd = auth()->user()->id;
       /* $trans = DB::SELECT("select *from activation_accounts a, users b, tabel_banks c, status d where a.user_id='$idd' and a.bank_id = c.id and a.status=d.id and a.user_id = b.id order by a.id DESC");*/
        $email = User::where('email', auth()->user()->email)->get();
        $trans = Activationaccount::with(['user','table_bank','statuss'])->where('user_id', $idd)->orderBy('id','DESC')->get();
        
        /*dd($coba[0]->statuss->name,$coba[0]->user->name);*/
        $details = [    'trans' => $trans[0],
                        'email' => $email,
                        'amount' => $amount,
                        'batas_p' => Carbon::now()->addDay(3),
                        'tglsekarang' => Carbon::now(),

                        
                    ];

        $pdf = PDF::loadView('page.dashboard.user.invoice.invoicetagihan', $details);

        $to_email = $email;
       /* Mail::to($to_email)->send(new \App\Mail\MyTestMailIndo($pdf));*/




        return redirect()->route('dashboard.',['trans' => $trans[0]]);
    }
}
