<?php

namespace App\Http\Controllers\Customer\api\v1\wallet;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Bankuser;
use App\Models\Images;
use App\Models\Kurs;
use App\Models\Status;
use App\Models\Tabelbank;
use App\Models\Topup;
use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\Wallethistory;
use App\Models\Walletmt5;
use App\Models\Walletmt5history;
use App\Models\Withdraw;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Facade\FlareClient\Api;
use Illuminate\Support\Facades\Auth;

class WalletController extends ApiController
{
    public function wallet()
    {
        $id = auth()->user()->id;
        $data = Wallet::where('user_id', $id)->first();
        return ApiController::success('Success', $data);
    }

    public function wallethistory()
    {
        $id = auth()->user()->id;
        $data = Wallethistory::with(['hystory_type_id', 'status'])->where('user_id', $id)->orderBy('created_at', 'DESC')->paginate();
        return ApiController::success('Success', $data);
    }
    public function walletmt5()
    {
        $id = auth()->user()->id;
        $data = Walletmt5::where('user_id', $id)->first();
        return ApiController::success('Success', $data);
    }

    public function walletmt5history()
    {
        $id = auth()->user()->id;
        $data = Walletmt5history::with(['hystory_type_id', 'status'])->where('user_id', $id)->orderBy('created_at', 'DESC')->paginate();
        return ApiController::success('Success', $data);
    }

    public function topup(Request $request)
    {
        $id = auth()->user()->id;
        $unik = rand(1, 999);
        $amount = $request->input('amount') + $unik;
        $kurs = Kurs::where('id', 1)->first();
        $amountdollar = (int)$request->input('amount') / $kurs->harga_jual;
        $bank_id = (int)$request->input('bank_id');
        $data = Topup::create([
            'user_id' => $id,
            'bank_id' => $bank_id,
            'amount' => $amount,
            'unik' => $unik,
            'status' => 10,
        ]);

        $wallet = walletmt5::where('user_id', $id)->first();

        // Walletmt5history::create([
        //     'user_id' => $id,
        //     'wallet_id' => $wallet->id,
        //     'hystory_type_id' => 3,
        //     'time' => Carbon::now(),
        //     'note' => 'Top-Up',
        //     'amount' => $amountdollar,
        //     'status' => 10,
        //     'token' => Str::random(60),
        // ]);
        $data->status = Status::where('id', $data->status)->first();
        $data->bank = Tabelbank::where('id', $data->bank_id)->first();

        return ApiController::success('Berhasil memasukkan Top-Up', $data);
    }

    public function topupupload(Request $request)
    {
        Topup::where('id', $request->input('id'))->update([
            'image_id' => $request->input('image_id'),
            'status' => 12,
        ]);
        $data = Topup::with(['status', 'bank'])->where('id', $request->input('id'))->first();

        return ApiController::success('Sukses upload bukti transfer!!', $data);
    }

    public function topuphistory(Request $request)
    {
        $id = auth()->user()->id;
        $data = Topup::with(['status', 'bank'])->where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        foreach ($data as $key => $value) {
            if ($value->image_id) {
                $data[$key]['image_url'] = Images::where('id', $value->image_id)->first()->image_url;
            }
        }
        return ApiController::success('Berhasil Mengambil history Top-Up', $data);
    }

    public function withdraw(Request $request)
    {
        $request->validate([
            'amount' => 'required',
            'bankuser_id' => 'required',
            'wallet_type' => 'required',
        ]);

        $id = auth()->user()->id;
        $bankuser_id = (int)$request->input('bankuser_id');
        if ($request->input('wallet_type') == 1) {
            $wallet_type = 16;
        }
        if ($request->input('wallet_type') == 2) {
            $wallet_type = 17;
        }
        $data = Withdraw::create([
            'user_id' => $id,
            'bankuser_id' => $bankuser_id,
            'amount' => $request->input('amount'),
            'wallet_type' => $wallet_type,
            'status' => 14,
        ]);
        $data->status = Status::where('id', $data->status)->first();
        $data->wallet_type = Status::where('id', $data->wallet_type)->first();
        $data->bank = Bankuser::where('id', $data->bankuser_id)->first();

        return ApiController::success('Berhasil memasukkan Withdraw', $data);
    }
    public function withdrawhistory(Request $request)
    {
        $id = auth()->user()->id;
        $data = Withdraw::where('user_id', $id)->get();
        $data = Withdraw::with(['status', 'bank', 'wallet_type'])->where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return ApiController::success('Berhasil Mengambil history Top-Up', $data);
    }
}
