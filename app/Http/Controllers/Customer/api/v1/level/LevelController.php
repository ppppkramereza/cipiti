<?php

namespace App\Http\Controllers\Customer\api\v1\level;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Levels;
use App\Models\User;
use Illuminate\Http\Request;

class LevelController extends ApiController
{
    public function level()
    {
        $data = Levels::get();
        return ApiController::success('Berhasil Get data Level', $data);
    }

    public function updatelevel(Request $request)
    {
        $id = auth()->user()->id;
        $data = User::where('id', $id)->update([
            'level_id' => $request->input('level_id'),
        ]);
        $data = User::find($id);
        return ApiController::success('Berhasil Ubah data Level user', $data);
    }
}
