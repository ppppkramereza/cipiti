<?php

namespace App\Http\Controllers\Customer\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Mail\ResetPasswordOTP;
use App\Models\Aktivasi;
use App\Models\Downline;
use App\Models\Images;
use App\Models\Status;
use App\Models\Upline;
use App\Models\Wallet;
use Carbon\Carbon;
use Facade\FlareClient\Api;
use GuzzleHttp\RetryMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Fortify\Rules\Password;
use laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Null_;
use Faker\Factory as Faker;

use function App\Helper\ReferralHelper\cektotalbawah;

class UserController extends ApiController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function fetch()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $level = User::with(['leveluser'])->where('id', auth()->user()->id)->first();
        $role = auth()->user()->roles[0];
        $user['level'] = $level->leveluser;
        $user['role'] = $role;
        // foreach ($user as $key => $value) {
        if ($user->image_id) {
            $user['image_url'] = Images::where('id', $user->image_id)->first()->image_url;
        }
        $user->level['image_url'] = Images::where('id', $user->level->image_id)->first()->image_url;

        // }

        return ApiController::success('Data profile user berhasil diambil', $user);
    }
    public function refflink()
    {
        $path_url = "https://dailypips.co/register/";
        $dat = $path_url . auth()->user()->refferal;
        if (auth()->user()->level_id < 3) {
            // $data = ['reflink' => 'maaf tidak dapat menampilkan refflink'];
            $data = ['reflink' => $dat];
        } else {
            $data = ['reflink' => $dat];
        }
        return ApiController::success('Data profile user berhasil diambil', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return ApiController::failure(

                    "Email Not Found",

                    "Email Not Found",
                    'Authentication Failed',
                    500
                );
            }
            if (!Hash::check($request->password, $user->password, [])) {
                // throw new \Exception('Invalid Credentials');
                return ApiController::failure(

                    " Wrong Password ",

                    " Wrong Password ",
                    'Authentication Failed',
                    500
                );
            }
            // auth()->user()->currentAccessToken()->delete();
            $user->tokens()->where('tokenable_id', $user->id)->delete();

            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return ApiController::failure(
                    'Unauthorized',
                    'Authentication Failed',
                    500
                );
            }


            $level = User::with(['leveluser'])->where('id', auth()->user()->id)->first();
            $role = auth()->user()->roles[0];
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            $user['access_token'] = $tokenResult;
            $user['token_type'] = 'Bearer';
            $user['level'] = $level->leveluser;
            $user['role'] = $role;
            if ($user->image_id) {
                $user['image_url'] = Images::where('id', $user->image_id)->first()->image_url;
            }
            $user->level['image_url'] = Images::where('id', $user->level->image_id)->first()->image_url;


            return ApiController::success('', $user, 'Authenticated');
        } catch (Exception $error) {
            $user['level_url'] = Images::where('id', $user->level->image_id)->first()->image_url;
            return ApiController::failure(
                'Something went wrong',

                'Authentication Failed',
                500
            );
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        // try {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            // 'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['nullable', 'string', 'max:255'],
            'country_code' => ['nullable', 'string', 'max:255'],
            'refferal' => ['required'],
            'password' => ['required']
        ]);
        $refferal = User::where('refferal', $request->refferal)->first();

        if (!$refferal) {
            return ApiController::failure('Error refferal tidak ada!!!', 'Error refferal tidak ada!!!');
        }

        $cekemail = User::where('email', $request->input('email'))->first();
        $cekphone = User::where('phone', $request->input('phone'))->first();

        if ($cekemail) {
            return ApiController::failure('Email Sudah Terdaftar!!', 'Email Sudah Terdaftar!!');
        }
        if ($cekphone) {
            return ApiController::failure('Nomor Telpon Sudah Terdaftar', 'Nomor Telpon Sudah Terdaftar');
        }

        $faker = Faker::create();

        $datauser = User::create([
            'name' => $request->name,
            // 'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'country_code' => $request->country_code,
            'password' => Hash::make($request->password),
            'refferal' => $faker->regexify('[A-Z0-9]{6}'),
            'status' => '1',
            'level_id' => '1',
        ]);
        $datauser->assignRole('User');

        Aktivasi::create([
            'upline_id' => $refferal->id,
            'user_id' => $datauser->id
        ]);

        $user = User::where('email', $request->email)->first();
        $otp = rand(100000, 999999);
        User::where('email', $request->input('email'))->update([
            'otp_regis' => $otp,
        ]);

        $data = [
            'status' => 1,
            'otp' => User::where('email', $request->input('email'))->first()->otp_regis
        ];

        $dataemail = [
            'email' => $request->email,
            'otp' => User::where('email', $request->email)->first()->otp_regis
        ];

        Mail::to($request->email)->send(new ResetPasswordOTP($dataemail));


        return ApiController::success(
            'Kode OTP berhasil dikirim di Email Anda, Cek Email anda sekarang!!',
            $data
        );
        // } catch (Exception $error) {
        //     return ApiController::failure([
        //         'Something went wrong',

        //         'Authentication Failed',
        //         500
        //     ], 'Authentication Failed', 500);
        // }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken()->delete();

        return ApiController::success('Token Revoked', $token);
    }

    public function cekemail(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        $email = User::where('email', $request->input('email'))->first();
        if (!$email) {
            $data = [
                'status' => 0,
                'otp' => (string)Carbon::now()->timestamp
            ];
            return ApiController::success('Email tidak ada', $data);
        }

        if ($email->email_verified_at == NULL) {


            $email = User::where('email', $request->input('email'))->first();

            if ($email) {
                if ($email->updated_at < Carbon::now()->addMinutes(-5)->toDateTimeString()) {

                    $otp = rand(100000, 999999);
                    User::where('email', $email->email)->update([
                        'otp_regis' => $otp,
                    ]);

                    $data = [
                        'status' => 1,
                        'otp' => User::where('email', $email->email)->first()->otp_regis
                    ];

                    $dataemail = [
                        'email' => $email->email,
                        'otp' => User::where('email', $email->email)->first()->otp_regis
                    ];

                    Mail::to($email->email)->send(new ResetPasswordOTP($dataemail));

                    return ApiController::success('OTP Dikirim ke email', $data);
                } else {
                    $otp = rand(100000, 999999);
                    User::where('email', $email->email)->update([
                        'otp_regis' => $otp,
                    ]);

                    $data = [
                        'status' => 1,
                        'otp' => User::where('email', $email->email)->first()->otp_regis
                    ];

                    $dataemail = [
                        'email' => $email->email,
                        'otp' => User::where('email', $email->email)->first()->otp_regis
                    ];

                    Mail::to($email->email)->send(new ResetPasswordOTP($dataemail));

                    return ApiController::success('OTP Dikirim ke email', $data);
                }
            }
        }
        if ($email->email_verified_at != NULL) {
            $data = [
                'status' => 2,
                'otp' => ''
            ];
            return ApiController::success('Beralih ke Form Login', $data);
        }
    }

    public function cekotp(Request $request)
    {
        $request->validate([
            'otp' => ['required'],
            'email' => ['required']
        ]);

        $user_id = User::where('email', $request->input('email'))->first()->id;
        $upline_aktivasi = Aktivasi::where('user_id', $user_id)->first()->upline_id;
        // dd($upline_aktivasi);
        $refferal = User::where('id', $upline_aktivasi)->first()->id;

        if (User::where('email', $request->input('email'))->first()->email_verified_at != NULL) {
            return ApiController::failure('Anda sudah ter aktivasi', 'Anda sudah ter aktivasi');
        }

        if ($request->input('otp') == User::where('email', $request->email)->first()->otp_regis && User::where('email', $request->email)->first()->status == 1) {
            User::where('email', $request->input('email'))->update([
                'email_verified_at' => Carbon::now(),
            ]);
            $datauser =  User::where('email', $request->input('email'))->first();

            $refferal = Aktivasi::where('user_id', $datauser->id)->first()->upline_id;
            Upline::create([
                'user_id' => $datauser->id,
                'upline_id' => $refferal,
            ]);

            Downline::create([
                'user_id' => $refferal,
                'downline_id' => $datauser->id,
            ]);

            Wallet::create([
                'user_id' => $datauser->id,
                'balance' => 0,
                'last_update' => Carbon::now(),
                'last_token' => Str::random(60),
            ]);
            cektotalbawah($datauser->id);

            $level = User::with(['leveluser'])->where('id', $datauser->id)->first();
            $role = User::with(['roles'])->where('id', $datauser->id)->first();
            $tokenResult = $datauser->createToken('authToken')->plainTextToken;
            $datauser['access_token'] = $tokenResult;
            $datauser['token_type'] = 'Bearer';
            $datauser['level'] = $level->leveluser;
            $datauser['role'] = $role->roles[0];
            if ($datauser->image_id) {
                $datauser['image_url'] = Images::where('id', $datauser->image_id)->first()->image_url;
            }
            $datauser->level['image_url'] = Images::where('id', $datauser->level->image_id)->first()->image_url;


            return ApiController::success('User Anda Berhasil Di Aktivasi', $datauser, 'Authenticated');
        }
        return ApiController::failure('OTP salah / User sudah ter aktivasi', 'OTP salah / User sudah ter aktivasi');
    }

    public function resendotp(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        $email = User::where('email', $request->input('email'))->first();
        if (!$email) {
            return ApiController::success('Email Tidak Ada/ Salah Email', 'Email Tidak Ada/ Salah Email');
        }

        if ($request->input('email')) {
            if ($email->updated_at < Carbon::now()->addMinutes(-5)->toDateTimeString()) {

                $otp = rand(100000, 999999);
                User::where('email', $request->input('email'))->update([
                    'otp_regis' => $otp,
                ]);
                $data = [
                    'status' => 1,
                    'otp' => User::where('email', $request->input('email'))->first()->otp_regis
                ];
                $dataemail = [
                    'email' => $request->input('email'),
                    'otp' => User::where('email', $request->input('email'))->first()->otp_regis
                ];

                Mail::to($email->email)->send(new ResetPasswordOTP($dataemail));
                return ApiController::success('Kode OTP', $data);
            }

            $data = [
                'status' => -1,
                'otp' => (string)Carbon::now()->timestamp
            ];
            return ApiController::success('Tunggu setelah 5 menit', $data);
        }
    }
    public function funresendotp($email1)
    {
        $email = User::where('email', $email1)->first();
        if (!$email) {
            return ApiController::success('Email Tidak Ada/ Salah Email', 'Email Tidak Ada/ Salah Email');
        }

        if ($email) {
            if ($email->updated_at < Carbon::now()->addMinutes(-5)->toDateTimeString()) {

                $otp = rand(100000, 999999);
                User::where('email', $email->email)->update([
                    'otp_regis' => $otp,
                ]);

                $data = [
                    'status' => 1,
                    'otp' => User::where('email', $email->email)->first()->otp_regis
                ];

                $dataemail = [
                    'email' => $email->email,
                    'otp' => User::where('email', $email->email)->first()->otp_regis
                ];

                Mail::to($email->email)->send(new ResetPasswordOTP($dataemail));

                // return ApiController::success('OTP Dikirim ke email', $data);
                return $this->$data['status'];
                // dd('123');
            }
            // dd('111123');
            return ApiController::success('Tunggu setelah 5 menit', 'Tunggu setelah 5 menit');
        }
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();

        $user = Auth::user();
        $user->update($data);

        return ApiController::success('Profile Updated', $user);
    }
}
