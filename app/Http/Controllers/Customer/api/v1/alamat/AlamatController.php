<?php

namespace App\Http\Controllers\Customer\api\v1\alamat;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Alamat;
use Illuminate\Http\Request;

class AlamatController extends ApiController
{

    public function createalamat(Request $request)
    {
        $id = auth()->user()->id;

        $request->validate([
            'alamat',
            'nohp',
            'kode_pos',
            'nama_penerima',
            'title',
        ]);

        $data = Alamat::create([
            'user_id' => $id,
            'alamat' => $request->input('alamat'),
            'nohp' => $request->input('nohp'),
            'kode_pos' => $request->input('kode_pos'),
            'nama_penerima' => $request->input('nama_penerima'),
            'title' => $request->input('title'),
            'last_action' => $id,
        ]);

        return ApiController::success('Berhasil Tambah Alamat', $data);
    }
    public function readalamat()
    {
        $id = auth()->user()->id;

        $data = Alamat::where('user_id', $id)->get();
        return ApiController::success('Berhasil Membaca Alamat', $data);
    }
    public function showalamat(Request $request)
    {
        // $id = auth()->user()->id;
        $request->validate([
            'id_alamat'
        ]);

        $data = Alamat::where('id', $request->input('id_alamat'))->first();
        return ApiController::success('Berhasil Membaca Alamat', $data);
    }
    public function updatealamat(Request $request)
    {
        $id = auth()->user()->id;
        $request->validate([
            'id_alamat',
            'alamat',
            'nohp',
            'kode_pos',
            'nama_penerima',
            'title',
        ]);
        $data = Alamat::where('user_id', $id)->where('id', $request->input('id_alamat'))->update([
            'user_id' => $id,
            'alamat' => $request->input('alamat'),
            'nohp' => $request->input('nohp'),
            'kode_pos' => $request->input('kode_pos'),
            'nama_penerima' => $request->input('nama_penerima'),
            'title' => $request->input('title'),
            'last_action' => $id,
        ]);
        return ApiController::success('Berhasil Edit Alamat');
    }
    public function deletealamat(Request $request)
    {
        $id = auth()->user()->id;

        $request->validate([
            'id_alamat',
        ]);

        $data = Alamat::where('user_id', $id)->where('id', $request->input('id_alamat'))->delete();
        return ApiController::success('Berhasil Delete Alamat');
    }
}
