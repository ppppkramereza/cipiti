<?php

namespace App\Http\Controllers\Customer\api\v1\robot;

use App\Http\Controllers\Controller;
use App\Models\Botprice;
use Illuminate\Http\Request;
use App\Helper\ResponseFormatter;
use App\Helper\ImageHelper;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Activationaccount;
use App\Models\Images;
use App\Models\Kurs;
use App\Models\Usdts;
use App\Models\Tabelbank;
use App\Models\User;
use Carbon\Carbon;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\Validator;


use function App\Helper\image;
use function App\Helper\ImageHelper\image as ImageHelperImage;
use function App\Helper\ReferralHelper\cekplusrobot;

class RobotController extends ApiController
{
    public function listrobot()
    {
        // $data = Botprice::get();
        $data = Botprice::get();
        foreach ($data as $key => $value) {
            if ($value->image_id) {
                $data[$key]['image_url'] = Images::where('id', $value->image_id)->first()->image_url;
            }
        }
        return ApiController::success('List Robot', $data);
    }

    public function belirobot(Request $request)
    {
        $id = auth()->user()->id;
        $databeli = Activationaccount::where('user_id', $id)->first();
        if ($databeli) {
            return ApiController::success('Your account is active', $databeli);
        }
        User::where('id', $id)->update(['status' => 2]);
        $kurs = Kurs::find(1);
        $hargabot = Botprice::find(1);
        // $kodeunik = $harga - $kurs;
        $kodeunik = rand(1, 999);
        $amount = $kurs->harga_jual * $hargabot->price + $kodeunik;
        // dd();
        $data = Activationaccount::create([
            'user_id' => $id,
            'alamat_id' => $request->input('alamat_id'),
            'amount' => $amount,
            'bank_id' => $request->input('bank_id'),
            'kurs_jual' => $kurs->harga_jual,
            'kode_unik' => $kodeunik,
            'status' => 4,
            'batas_pembayaran' => Carbon::now()->addDay(3),
            'bot_exp' => Carbon::now()->addDay(365),
        ]);
        if ($data->image_id) {
            $data->image_url = Images::where('id', $data->image_id)->first()->image_url;
        }
        $data->bank = Tabelbank::where('id', $request->input('bank_id'))->first();
        return ApiController::success('Success request order, please confirm your transfer receipt', $data);
        // return redirect()->route('dashboard.dev.bypassrobot.index');
    }

    public function belirobotbypass(Request $request)
    {
        $id = $request->input('user_id');
        $databeli = Activationaccount::where('user_id', $id)->first();
        if ($databeli) {
            return ApiController::success('Your account is active', $databeli);
        }
        User::where('id', $id)->update(['status' => 2]);
        $kurs = Kurs::find(1);
        $hargabot = Botprice::find(1);
        // $kodeunik = $harga - $kurs;
        $kodeunik = rand(1, 999);
        $amount = $kurs->harga_jual * $hargabot->price + $kodeunik;
        // dd();
        $data = Activationaccount::create([
            'user_id' => $id,
            'alamat_id' => 1,
            'amount' => $amount,
            'bank_id' => '1',
            'kurs_jual' => $kurs->harga_jual,
            'kode_unik' => $kodeunik,
            'status' => 5,
            'batas_pembayaran' => Carbon::now()->addDay(3),
            'image_id' => 'level7',
            'bot_exp' => Carbon::now()->addDay(365),
        ]);
        if ($data->image_id) {
            $data->image_url = Images::where('id', $data->image_id)->first()->image_url;
        }
        cekplusrobot($id);
        $data->bank = Tabelbank::where('id', $request->input('bank_id'))->first();
        return ApiController::success('Pebelian Berhasil', $data);
    }

    public function statusbelirobot()
    {
        $id = auth()->user()->id;
        $data = Activationaccount::where('user_id', $id)->first();
        if (!$data) {
            return ApiController::notFound();
        }
        $bank = Tabelbank::where('id', $data->bank_id)->first();
        if ($bank) {
            $data->bank = $bank;
        }
        if ($data->status == 4) {
            return ApiController::success('Status Robot belum terbayar', $data);
        }
        if ($data->image_id) {
            $data->image_url = Images::where('id', $data->image_id)->first()->image_url;
        }
        return ApiController::success('Status Robot Sudah terbayar', $data);
    }

    // public function uploadimagerobot(Request $request)
    // {
    //     // dd($request->file('image'));
    //     // dd('asd');
    //     $data = [
    //         'image' => $request->file('image')->getClientOriginalName(),
    //     ];
    //     ImageHelper::image('robot', $request->file('image'));
    //     return ApiController::success('Succes Upload', $data);
    // }

    public function robotupload(Request $request)
    {
        $id = auth()->user()->id;
        Activationaccount::where('user_id', $id)->update([
            'image_id' => $request->input('image_id'),
        ]);
        $data = Activationaccount::where('user_id', $id)->first();

        return ApiController::success('Success Upload transfer receipt', $data);
    }
}
