<?php

namespace App\Http\Controllers\Customer\api\v1\tac;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Aktivasi;
use App\Models\Tac;
use App\Models\User;
use Facade\FlareClient\Api;
use Illuminate\Http\Request;

class TacController extends ApiController
{
    public function tacaktivasi(Request $request)
    {
        $request->validate([
            'tac' => ['required']
        ]);


        $tac_id = Tac::where('kode_produksi', $request->input('tac'))->first();
        if (!$tac_id) {
            $tac_id = Tac::where('tac_1', $request->input('tac'))->first();
            if (!$tac_id) {
                $tac_id = Tac::where('tac_2', $request->input('tac'))->first();

                if (!$tac_id) {
                    $tac_id = Tac::where('tac_3', $request->input('tac'))->first();

                    if (!$tac_id) {
                        $tac_id = Tac::where('tac_4', $request->input('tac'))->first();
                    }
                }
            }
        }

        $userstatus = User::where('id', auth()->user()->id)->first();
        $userstatus = $userstatus->status;

        if ($userstatus != 2) {
            return ApiController::failure('Tidak Memenuhi Syarat Aktivasi', 'Tidak Memenuhi Syarat Aktivasi');
        }

        $tac_aktivasi = Aktivasi::where('tac_id', $tac_id->id)->first();

        if ($tac_aktivasi) {
            return ApiController::success('Kode Tac sudah dipakai', $tac_aktivasi);
        } else {
            $data_aktivasi = Aktivasi::where('user_id', auth()->user()->id)->update([
                'tac_id' => $tac_id->id
            ]);
            User::where('id', auth()->user()->id)->update([
                'status' => 3
            ]);
            return ApiController::success('User Berhasil Aktivasi Dengan TAC');
        }
    }

    public function tacdetail()
    {
        $id = auth()->user()->id;
        $datatac = Aktivasi::where('user_id', $id)->first();
        if (!$datatac) {
            return ApiController::notfound();
        }
        $datatac = $datatac->tac_id;
        $data = Tac::where('id', $datatac)->first();
        return ApiController::success('Get Data Tac', $data);
    }
}
