<?php

namespace App\Http\Controllers\Customer\api\v1\phonecode;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\PhoneCountrie;
use Illuminate\Http\Request;

class PhoneCodeController extends ApiController
{
    public function getphonecode()
    {
        $data = PhoneCountrie::get();

        return ApiController::success('Berhasil Get data PhoneCode', $data);
    }
}
