<?php

namespace App\Http\Controllers\Customer\api\v1\profile;

use App\Helper\ImageHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Mail\MyTestMail;
use App\Mail\MyTestMailIndo;
use App\Mail\ResetPasswordOTP;
use App\Models\Downline;
use App\Models\Images;
use App\Models\Passwordreset;
use App\Models\PhoneCountrie;
use App\Models\Upline;
use App\Models\Wallet;
use App\Models\Walletmt5;
use Carbon\Carbon;
use Facade\FlareClient\Api;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\Else_;
use Svg\Tag\Rect;
use Symfony\Component\Console\Input\Input;

class ProfileController extends ApiController
{

    public function index()
    {
        $data = User::get();
        return ApiController::success('Data Berhasil di tarik!!', $data);
    }

    public function countrycode()
    {
        $data = PhoneCountrie::get();
        return ApiController::success('Get Country code', $data);
    }

    public function register(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = Str::random(8);
        $passwordhash = Hash::make($password);

        $country_code = $request->country_code;
        $phone = (int)$request->phone;
        $refferal = Str::random(8) . Carbon::now()->timestamp . Str::random(8);
        $refferaluplines = $request->refferalup;

        $checkphone = $phone;
        $sub_checkphone = substr($checkphone, 0, 1);


        $cim = DB::SELECT("select*from users where email='$email'");
        $cip = DB::SELECT("select*from users where phone='$phone'");
        $cid = DB::SELECT("select*from users where refferal='$refferaluplines'");
        if ($cim == null) {
            if ($cip == null) {
                if ($cid == null) {
                    return ApiController::failure('error', 'Refferal yang digunakan tidak tersedia');
                } else {

                    $data = User::create([
                        'name' => $name,
                        'email' => $email,
                        'country_code' => $country_code,
                        'phone' => $phone,
                        'refferal' => $refferal,
                        'password' => $passwordhash,
                        'status' => 1,
                        'level_id' => 1,
                        'total_robot' => 0,
                    ]);
                    $data->assignrole('Customer');

                    Wallet::create([
                        'user_id' => $data->id,
                        'balance' => 0,
                        'last_update' => Carbon::now(),
                        'last_token' => Str::random(60),
                    ]);

                    Walletmt5::create([
                        'user_id' => $data->id,
                        'balance' => 0,
                        'last_update' => Carbon::now(),
                        'last_token' => Str::random(60),
                    ]);

                    //$checkupl = DB::SELECT("select*from users a, uplines b where a.id = b.user.id and a.id = b.upline_id");
                    $checkupl = Upline::create([
                        'user_id' => $data->id,
                        'upline_id' => $cid[0]->id

                    ]);
                    $checkudl = Downline::create([
                        'user_id' => $cid[0]->id,
                        'downline_id' => $data->id

                    ]);


                    $details = [
                        'body' => $name,
                        'pass' => $password,
                        'email' => $email
                    ];

                    if ($country_code == 62) {
                        Mail::to($email)->send(new MyTestMailIndo($details));
                    } else {
                        Mail::to($email)->send(new MyTestMail($details));
                    }
                    return ApiController::success('Berhasil Terkirim ke email');
                }
            } else {
                return ApiController::failure('error', 'Nomor yang digunakan telah terdaftar / Format Penulisan Salah');
            }
        } else {
            return ApiController::failure('error', 'Email yang digunakan telah terdaftar');
        }
    }

    public function edit(Request $request)
    {
        $id = auth()->user()->id;

        $phone = (int)$request->input('phone');
        $ceknomor = User::where('phone', $phone)->first();

        if ($ceknomor->phone == $phone) {
            $data = User::where('id', $id)->update([
                'name' => $request->input('name'),
                'phone' =>  $phone,
            ]);

            $data = User::where('id', $id)->first();

            if ($request->file('image')) {
                $idimage = Str::uuid();
                ImageHelper::image('user', $request->file('image'), $idimage);

                User::where('id', $id)->update([
                    'image_id' => $idimage,
                ]);

                $data->image = Images::where('id', $idimage)->first()->image_url;
            }

            return ApiController::success('Berhasil Edit Profile', $data);
        } elseif ($ceknomor) {
            return ApiController::failure('error', 'Nomor Sudah Terdaftar');
        }
    }

    public function editpassword(Request $request)
    {
        $id = auth()->user()->id;

        $passwordold = User::where('id', $id)->first()->password;
        $passwordnew = Hash::make($request->input('newpassword'));

        if (Hash::check($request->input('password'), auth()->user()->password)) {
            User::where('id', $id)->update([
                'password' => $passwordnew,
            ]);
        } else {
            return ApiController::failure('error', 'Password Lama tidak sesuai!!');
        }

        return ApiController::success('Password Berhasil Diubah!!!');
    }

    public function resetpassword(Request $request)
    {
        $request->validate(
            [
                'email' => ['required', 'email'],
            ]
        );
        $emailuser = User::where('email', $request->input('email'))->first();

        if (!$emailuser) {
            return ApiController::failure('Error', 'Email Not Found');
        }

        $data = Passwordreset::where('email', $request->input('email'))->delete();

        $otp = rand(100000, 999999);

        Passwordreset::create([
            'email' => $emailuser->email,
            'token' => $otp,
            'created_at' => Carbon::now(),

        ]);

        $dataemail = [
            'email' => $emailuser->email,
            'otp' => $otp
        ];

        $data = [
            'kode_otp' => $otp,
            'email' => $emailuser->email,
        ];

        Mail::to($emailuser->email)->send(new ResetPasswordOTP($dataemail));

        return ApiController::success('Kode OTP berhasil dikirim di Email Anda, Cek Email anda sekarang!!', $data);
    }

    public function editpasswordotp(Request $request)
    {
        $request->validate([
            'email',
            'otp',
            'password'
        ]);

        $passwordreset = Passwordreset::where('email', $request->input('email'))->first();
        if (!$passwordreset) {
            return ApiController::failure('Email tidak ditemukan dipermintaan reset password', null);
        }

        if ($request->input('email') == $passwordreset->email) {
            if ($request->input('otp') == $passwordreset->token) {
                User::where('email', $request->input('email'))->update([
                    'password' => Hash::make($request->input('password'))
                ]);
                Passwordreset::where('email', $request->input('email'))->delete();
                return Apicontroller::success('Berhasil merubah password anda');
            } else {
                return ApiController::failure('OTP Salah atau tidak sesuai dengan otp yang dikirim', null);
            }
        } else {
            return ApiController::failure('Email tidak ditemukan dipermintaan reset password', null);
        }
    }
}
