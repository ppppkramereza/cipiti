<?php

namespace App\Http\Controllers\Customer\api\v1\totalbawah;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Activationaccount;
use App\Models\Totalbawah;
use Illuminate\Http\Request;

class TotalbawahController extends ApiController
{
    public function list()
    {
        $id = auth()->user()->id;
        // $id = 3;
        $data = Totalbawah::with(['user_up', 'user_baru'])->where('user_up', $id)->get();
        if (!$data) {
            $data = [
                'Total' => 0
            ];
        }
        return ApiController::success('Berhasil Get list', $data);
    }
    public function count()
    {
        $id = auth()->user()->id;
        // $id = 3;
        $total = Totalbawah::where('user_up', $id)->get();

        if (!$total) {

            $data = [
                'Total' => 0
            ];
            return ApiController::success('', $data);
        }

        $return = [];

        foreach ($total as $key => $value) {
            $a = $value->user_baru;
            $b = Activationaccount::where('user_id', $a)->where('status', 5)->first();
            if ($b != NULL) {
                array_push($return, $value);
            }
        }

        $level1 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 1);
        })->count();
        $level2 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 2);
        })->count();
        $level3 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 3);
        })->count();
        $level4 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 4);
        })->count();
        $level5 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 5);
        })->count();
        $level6 = Totalbawah::with(['user'])->where('user_up', $id)->whereHas('user', function ($q) {
            $q->where('level_id', '=', 6);
        })->count();


        $data = [
            'total' => count($return),
            'level1' => 0,
            'level2' => 0,
            'level3' => 0,
            'level4' => 0,
            'level5' => 0,
            'level6' => 0,
        ];

        return ApiController::success('Berhasil Get count', $data);
    }
}
