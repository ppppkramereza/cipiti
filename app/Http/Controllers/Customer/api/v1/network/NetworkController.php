<?php

namespace App\Http\Controllers\Customer\api\v1\network;

use App\Http\Controllers\Controller;
use App\Models\Upline;
use Illuminate\Http\Request;
use App\Helper\ResponseFormatter;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Activationaccount;
use App\Models\Downline;

class NetworkController extends ApiController
{

    public function upline()
    {
        $id = auth()->user()->id;
        $data = Upline::with(['user', 'upline'])->where('user_id', $id)->first();
        return ApiController::success('Data Upline', $data);
    }
    public function downline()
    {
        $id = auth()->user()->id;
        $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();
        $data->total_downline =  Downline::with(['user', 'downline'])->where('user_id', $id)->count();
        return ApiController::success('Data Downline', $data);
    }

    //dipakek
    public function downlineid(Request $request)
    {
        $id = $request->input('user_id');
        $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();
        $return = [];

        foreach ($data as $key => $value) {
            $a = $value->downline->id;
            $b = Activationaccount::where('user_id', $a)->where('status', 5)->first();
            if ($b != NULL) {
                array_push($return, $value);
            }
        }
        $dat =  Downline::with(['downline'])->where('user_id', $id)->count();
        foreach ($return as $key => $value) {
            // $value->total_downline =  Downline::with(['downline'])->where('user_id', $value->downline_id)->count();
            $z = Downline::with(['downline'])->where('user_id', $value->downline_id)->get();
            $returnz = [];

            foreach ($z as $key => $value2) {
                $a = $value2->downline->id;
                $b = Activationaccount::where('user_id', $a)->where('status', 5)->first();
                if ($b != NULL) {
                    array_push($returnz, $value2);
                }
            }
            $value->total_downline = count($returnz);
        }
        // $return[0]['total_downline'] = count($return);
        return ApiController::success('Data Downline', $return);
    }
    public function directsponsor()
    {
        $id = auth()->user()->id;
        $data = Downline::with(['user', 'downline'])->where('user_id', $id)->get();
        $return = [];

        foreach ($data as $key => $value) {
            $a = $value->downline->id;
            $b = Activationaccount::where('user_id', $a)->where('status', 5)->first();
            if ($b != NULL) {
                array_push($return, $value);
            }
        }
        $total = count($return);
        $datas = ['Total' => $total];
        return ApiController::success('Total pengguna referral', $datas);
    }
    public function downlinebot()
    {
        $id = auth()->user()->id;
        $data = Downline::with(['downlinebot'])->where('user_id', $id)->get();
        return ApiController::success('Total pengguna referral', $data);
    }
}
