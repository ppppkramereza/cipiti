<?php

namespace App\Http\Controllers\Customer\api\v1\image;

use App\Helper\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Customer\api\ApiController;
use App\Models\Images;
use App\Models\Topup;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ImageController extends ApiController
{

    function upload(Request $request)
    {
        $folder = $request->input('folder');

        $validFolder = [
            'robot',
            'topup',
            'user',
        ];

        if (!in_array($folder, $validFolder)) {
            return ApiController::failure('Folder Upload not permession', 'Folder Upload not permession');
        }

        $image = $request->file('image');
        $id = Str::uuid();
        ImageHelper::image($folder, $image, $id);

        $data = Images::where('id', $id)->first();


        return ApiController::success('Success Upload Image', $data);
    }
}
