<?php

namespace App\Http\Controllers\Customer\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\EncapsulatedApiResponder;
use Illuminate\Contracts\Validation\Validator;

class ApiController extends Controller
{
    protected function customValidation(Request $request, $rules = [], $messages = [])
    {
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->invalidParameters($validator->errors()->all());
        } else {
            return TRUE;
        }
    }

    protected function guardWithValidation(Request $request, $rules = [], $messages = [], $callback)
    {
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return $this->invalidParameters($validator->errors()->all());
        } else {
            return $callback();
        }
    }

    protected $responseFormat = [
        'response_code' => NULL,
        'message' => NULL,
        'errors' => NULL,
        'data' => NULL
    ];

    protected function success($message = 'Successed.', $data = NULL)
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 200,
            'message' => $message,
            'data' => $data,
        ]));
    }

    protected function failure($message, $errors)
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 400,
            'message' => $message,
            'errors' => $errors,
        ]), 200);
    }

    protected function unauthorized()
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 401,
            'errors' => 'unauthorized',
            'Invalid token.',
        ]), 200);
    }

    protected function forbidden()
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 403,
            'errors' => 'forbidden',
            'Forbidden.',
        ]), 403);
    }

    protected function notFound($errors = 'Not found.')
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 404,
            'errors' => is_null($errors) ? $errors : ($errors ? $errors : $errors),
        ]), 404);
    }

    protected function invalidParameters($errors)
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => 422,
            'errors' => is_null($errors) ? $errors : ($errors ? $errors : $errors),
        ]), 422);
    }

    protected function customResponse($responseCode = 200, $message = '', $errors = NULL, $data = NULL)
    {
        return response()->json(array_merge($this->responseFormat, [
            'response_code' => $responseCode,
            'message' => $message,
            'errors' => is_null($errors) ? $errors : ($errors ? $errors : $errors),
            'data' => $data,
        ]));
    }
}
