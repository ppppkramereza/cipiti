<?php

namespace App\Http\Controllers\Customer\api;

use App\Http\Controllers\Controller;
use App\Models\VersionApp;
use Illuminate\Http\Request;

class AppController extends ApiController
{
    public function versionapp()
    {
        $data = VersionApp::first();
        $data = $data->versi_apps;

        return ApiController::success('Berhasil Ambil Versi', $data);
    }
}
