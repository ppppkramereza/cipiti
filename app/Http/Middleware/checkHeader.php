<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Customer\api\ApiController;
use App\Models\VersionApp;
use Closure;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;

class checkHeader extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!isset($_SERVER['HTTP_VERSION'])) {
            return ApiController::failure(
                "Auth Header Err!!",

                "Auth Header Err!!",

            );
        }
        $data = VersionApp::first();
        $data = $data->versi_apps;
        if ($_SERVER['HTTP_VERSION'] < $data) {
            return ApiController::failure(
                "Silahkan Update Aplikasi Anda!",

                "Silahkan Update Aplikasi Anda!",

            );
        }

        return $next($request);
    }
}
