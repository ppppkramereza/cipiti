<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historymoota extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'history_mootas';

    protected $fillable = [
        'id_moota',
        'date',
        'type',
        'amount',
        'balance',
        'bank_id',
        'bank_type',
        'description',
        'account_number',
    ];
}
