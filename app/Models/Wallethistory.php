<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallethistory extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'wallet_historys';

    protected $fillable = [
        'user_id',
        'wallet_id',
        'hystory_type_id',
        'time',
        'note',
        'amount',
        'token',
        'status'
    ];

    public function hystory_type_id()
    {
        return $this->belongsTo(Historytype::class, 'hystory_type_id', 'id');
    }
    public function status()
    {
        return $this->belongsTo(Status::class, 'status', 'id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
