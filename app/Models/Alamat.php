<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alamat extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'alamats';

    protected $fillable = [
        'user_id',
        'alamat',
        'nohp',
        'kode_pos',
        'nama_penerima',
        'title',
        'last_action',
    ];
}
