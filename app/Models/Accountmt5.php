<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accountmt5 extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'account_mt5';

    protected $fillable = [
        'user_id',
        'username',
        'password',
        'server',
        'ip_server',
        'password_investor',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
