<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mutasi_history extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'mutasi_history';

    protected $fillable = [
        'user_id',
        'account_number',
        'date',
        'description',
        'amount',
        'type',
        'note',
        'balance',
        'created_at_moota',
        'updated_at_moota',
        'status',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
