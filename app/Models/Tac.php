<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tac extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tacs';

    protected $fillable = [
        'kode_produksi',
        'tac_1',
        'tac_2',
        'tac_3',
        'tac_4'
    ];
}
