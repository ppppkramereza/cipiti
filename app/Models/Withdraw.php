<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'withdraws';

    protected $fillable = [
        'user_id',
        'bankuser_id',
        'amount',
        'status',
        'wallet_type'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status', 'id');
    }
    public function wallet_type()
    {
        return $this->belongsTo(Status::class, 'wallet_type', 'id');
    }
    public function bank()
    {
        return $this->belongsTo(Bankuser::class, 'bankuser_id', 'id');
    }
}
