<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBuy extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'user_buys';
    protected $fillable = [
        'user_id',
        'user_id_buy'
    ]
}
