<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'country_code',
        'phone',
        'refferal',
        'level_id',
        'status',
        'modal_user',
        'total_robot',
        'two_factor_secret',
        'two_factor_recovery_codes'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function upline()
    {
        return $this->hasMany(upline::class, 'upline_id', 'id');
    }
    // public function upline()
    // {
    //     return $this->hasMany(upline::class, 'upline_id', 'id');
    // }
    public function downline()
    {
        return $this->belongsTo(downline::class, 'downline_id', 'id');
    }

    public function leveluser()
    {
        return $this->hasOne(Levels::class, 'id', 'level_id');
    }

    public function statususer()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }
    public function mt5()
    {
        return $this->hasOne(Walletmt5::class, 'user_id', 'id');
    }
    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id', 'id');
    }
}
