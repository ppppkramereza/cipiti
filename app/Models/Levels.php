<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Levels extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'levels';

    protected $fillable = [
		'name',
	    'direct_bonus',
	    'sponsor_request',
	    'bot_request',
	    'depo_minimum',
	    'wd_maximum',
	    'profit_share',
	    'lvl',
	];
    public function leveluser()
    {
        return $this->hasMany(User::class, 'level_id', 'id');
    }
}
