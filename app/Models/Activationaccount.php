<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activationaccount extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'activation_accounts';

    protected $fillable = [
        'user_id',
        'alamat_id',
        'amount',
        'bank_id',
        'kurs_jual',
        'kode_unik',
        'status',
        'batas_pembayaran',
        'bot_exp',
        'image_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function table_bank()
    {
        return $this->hasOne(Tabelbank::class, 'id', 'bank_id');
    }
    public function statuss()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }
}
