<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kurs extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'kurs';

    protected $fillable = [
        'nama',
        'harga_jual',
        'harga_beli',
    ];
}
