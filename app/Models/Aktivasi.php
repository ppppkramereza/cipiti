<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aktivasi extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'aktivasis';
    protected $fillable = [
        'tac_id',
        'upline_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'upline_id', 'id');
    }

    public function tac()
    {
        return $this->belongsTo(Tac::class, 'tac_id', 'id');
    }
}
