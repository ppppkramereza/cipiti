<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listcoin extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'list_coins';
    protected $fillable = [
        'name',
    ];
}
