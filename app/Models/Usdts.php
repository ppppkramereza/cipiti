<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usdts extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'usdts';
    protected $fillable = [
        'harga_jual',
        'harga_beli',
    ];
}
