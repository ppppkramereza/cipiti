<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keuntungan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'keuntungans';

    protected $fillable = [
        'tambah',
        'nilai_awal',
        'nilai_akhir',
        'from',
        'to',
    ];
}
