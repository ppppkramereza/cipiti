<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bankuser extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'bank_users';

    protected $fillable = [
        'bank_id',
        'account_name',
        'bank_rek',
        'user_id',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo(Bankkode::class, 'bank_id', 'id');
    }
}
