<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Totalbawah extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'total_bawahs';
    protected $fillable = [
        'user_up',
        'user_baru'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_up', 'id');
    }
    public function user_up()
    {
        return $this->belongsTo(User::class, 'user_up', 'id');
    }
    public function user_baru()
    {
        return $this->belongsTo(User::class, 'user_baru', 'id');
    }
}
