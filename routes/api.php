<?php

use App\Http\Controllers\Customer\api\AppController;
use App\Http\Controllers\Customer\api\v1\alamat\AlamatController;
use App\Http\Controllers\Customer\api\v1\bank\BankController;
use App\Http\Controllers\Customer\api\v1\image\ImageController;
use App\Http\Controllers\Customer\api\v1\kurs\KursController;
use App\Http\Controllers\Customer\api\v1\level\LevelController;
use App\Http\Controllers\Customer\api\v1\mt5\mt5Controller;
use App\Http\Controllers\Customer\api\v1\network\NetworkController;
use App\Http\Controllers\Customer\api\v1\phonecode\PhoneCodeController;
use App\Http\Controllers\Customer\api\v1\profile\ProfileController;
use App\Http\Controllers\Customer\api\v1\profit\ProfitController;
use App\Http\Controllers\Customer\api\v1\robot\RobotController;
use App\Http\Controllers\Customer\api\v1\tac\TacController;
use App\Http\Controllers\Customer\api\v1\totalbawah\TotalbawahController;
use App\Http\Controllers\Customer\api\v1\UserController;
use App\Http\Controllers\Customer\api\v1\wallet\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Profiler\Profile;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user()->currentAccessToken()->tokenable_id;
    // return $request->user()->tokens()->delete();
});

Route::post('/belirobotbypass', [RobotController::class, 'belirobotbypass'])->name('api.belirobotbypass');

Route::middleware('checkheader')->group(function () {

    Route::name('v1.auth')->prefix('v1/auth')->group(
        function () {
            Route::post('login', [UserController::class, 'login'])->name('login');
            Route::post('register', [UserController::class, 'register'])->name('register');
            Route::post('logout', [UserController::class, 'logout'])->name('logout')->middleware(['auth:sanctum']);
            Route::get('fetch', [UserController::class, 'fetch'])->name('fetch')->middleware(['auth:sanctum']);
            Route::get('refflink', [UserController::class, 'refflink'])->name('refflink')->middleware(['auth:sanctum']);
            Route::post('cekemail', [UserController::class, 'cekemail'])->name('cekemail');
            Route::post('cekotp', [UserController::class, 'cekotp'])->name('cekotp');
            Route::post('resendotp', [UserController::class, 'resendotp'])->name('resendotp');
        }
    );

    Route::name('v1.user')->prefix('v1/user')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
            Route::get('phone', [ProfileController::class, 'countrycode'])->name('profile.countrycode');
            Route::post('register', [ProfileController::class, 'register'])->name('profile.register');
            Route::get('mt5get', [mt5Controller::class, 'get'])->name('mt5.get');
            Route::post('mt5update', [mt5Controller::class, 'update'])->name('mt5.update');
            Route::post('edit-profile', [ProfileController::class, 'edit'])->name('update.profile');
            Route::post('edit-password', [ProfileController::class, 'editpassword'])->name('update.password');
            Route::post('reset-password', [ProfileController::class, 'resetpassword'])->name('reset.password');
        }
    );
    Route::name('v1.otp')->prefix('v1/otp')->middleware([])->group(
        function () {
            Route::post('reset-password', [ProfileController::class, 'resetpassword'])->name('reset.password');
            Route::post('edit-password', [ProfileController::class, 'editpasswordotp'])->name('edit.password');
        }
    );

    Route::name('v1.robot')->prefix('v1/robot')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('list-robot', [RobotController::class, 'listrobot'])->name('list.robot.index');
            Route::post('beli-robot', [RobotController::class, 'belirobot'])->name('beli.robot.index');
            Route::post('status-beli-robot', [RobotController::class, 'statusbelirobot'])->name('status.beli.robot.index');
            Route::post('upload-image', [RobotController::class, 'uploadimagerobot'])->name('upload.image.robot.index');
            Route::post('robotupload', [RobotController::class, 'robotupload'])->name('robot.upload');
        }
    );

    Route::name('v1.wallet')->prefix('v1/wallet')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('wallet', [WalletController::class, 'wallet'])->name('wallet');
            Route::post('wallet-history', [WalletController::class, 'wallethistory'])->name('wallet.history');
            Route::post('walletmt5', [WalletController::class, 'walletmt5'])->name('walletmt5');
            Route::post('walletmt5-history', [WalletController::class, 'walletmt5history'])->name('walletmt5.history');
            Route::post('topup', [WalletController::class, 'topup'])->name('topup');
            Route::post('topup-history', [WalletController::class, 'topuphistory'])->name('topup.history');
            Route::post('topupupload', [WalletController::class, 'topupupload'])->name('topup.upload');
            Route::post('withdraw', [WalletController::class, 'withdraw'])->name('withdraw');
            Route::post('withdraw-history', [WalletController::class, 'withdrawhistory'])->name('withdraw.history');
        }
    );
    Route::name('v1.bank')->prefix('v1/bank')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('list-bank', [BankController::class, 'listbank'])->name('bank');
            Route::get('metode-pembayaran', [BankController::class, 'metodepembayaran'])->name('metode.pembayaran');
            Route::post('upload-image', [BankController::class, 'uploadimagebank'])->name('upload.image.bank.index');
            Route::get('getbankkode', [BankController::class, 'getbankkode'])->name('get.bank.kode');
            Route::get('getbankuser', [BankController::class, 'getbankuser'])->name('get.bank.user');
            Route::post('addbankuser', [BankController::class, 'addbankuser'])->name('add.bank.user');
            Route::post('updatebankuser', [BankController::class, 'updatebankuser'])->name('update.bank.user');
            Route::post('deletebankuser', [BankController::class, 'deletebankuser'])->name('delete.bank.user');
        }
    );
    Route::name('v1.network')->prefix('v1/network')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('upline', [NetworkController::class, 'upline'])->name('upline');
            Route::post('downline', [NetworkController::class, 'downlineid'])->name('downlineid');
            Route::get('direct-sponsor', [NetworkController::class, 'directsponsor'])->name('direct.sponsor');
            Route::get('downline-bot', [NetworkController::class, 'downlinebot'])->name('downline.bot');
        }
    );
    Route::name('v1.kurs')->prefix('v1/kurs')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('kurs', [KursController::class, 'kurs'])->name('kurs');
        }
    );
    Route::name('v1.profit')->prefix('v1/profit')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('profit', [ProfitController::class, 'profit'])->name('profit');
        }
    );
    Route::name('v1.image')->prefix('v1/image')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('upload', [ImageController::class, 'upload'])->name('upload');
        }
    );

    Route::name('v1.level')->prefix('v1/level')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('level', [LevelController::class, 'level'])->name('level');
            Route::post('update-level', [LevelController::class, 'updatelevel'])->name('update.level');
        }
    );
    Route::name('v1.code')->prefix('v1/code')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('code', [PhoneCodeController::class, 'getphonecode'])->name('code');
        }
    );
    Route::name('v1.tac')->prefix('v1/tac')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('tac-aktivasi', [TacController::class, 'tacaktivasi'])->name('tac.aktivasi');
            Route::get('tac-detail', [TacController::class, 'tacdetail'])->name('tac.detail');
        }
    );
    Route::name('v1.alamat')->prefix('v1/alamat')->middleware(['auth:sanctum'])->group(
        function () {
            Route::post('alamat-create', [AlamatController::class, 'createalamat'])->name('alamat.create');
            Route::get('alamat-read', [AlamatController::class, 'readalamat'])->name('alamat.read');
            Route::post('alamat-show', [AlamatController::class, 'showalamat'])->name('alamat.show');
            Route::post('alamat-update', [AlamatController::class, 'updatealamat'])->name('alamat.update');
            Route::post('alamat-delete', [AlamatController::class, 'deletealamat'])->name('alamat.delete');
        }
    );
    // Route::name('v1.alamat')->prefix('v1/alamat')->middleware(['auth:sanctum'])->group(
    //     function () {
    //         Route::post('alamat-create', [AlamatController::class, 'createalamat'])->name('alamat.create');
    //         Route::get('alamat-read', [AlamatController::class, 'readalamat'])->name('alamat.read');
    //         Route::post('alamat-show', [AlamatController::class, 'showalamat'])->name('alamat.show');
    //         Route::post('alamat-update', [AlamatController::class, 'updatealamat'])->name('alamat.update');
    //         Route::post('alamat-delete', [AlamatController::class, 'deletealamat'])->name('alamat.delete');
    //     }
    // );
    Route::name('v1.totalbawah')->prefix('v1/totalbawah')->middleware(['auth:sanctum'])->group(
        function () {
            Route::get('totalbawah-list', [TotalbawahController::class, 'list'])->name('list');
            Route::get('totalbawah-count', [TotalbawahController::class, 'count'])->name('count');
        }
    );


    Route::name('version')->prefix('version')->middleware([])->group(
        function () {
            Route::get('version', [AppController::class, 'versionapp'])->name('version');
        }
    );
});
