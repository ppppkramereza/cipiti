<?php

use App\Http\Controllers\customer\RobotController;
use App\Http\Controllers\Dev\MutationController as DevMutationController;
use App\Http\Controllers\Dev\UserController as DevUserController;
use App\Http\Controllers\Dev\BankController as DevBankController;
use App\Http\Controllers\Dev\LevelController as DevLevelController;
use App\Http\Controllers\Dev\RobotController as DevRobotController;
use App\Http\Controllers\Dev\KursController as DevKursController;
use App\Http\Controllers\Dev\ContactController as DevContactController;
use App\Http\Controllers\Dev\TopupController as DevTopupController;
use App\Http\Controllers\Dev\MemberBankController as DevMemberBankController;
use App\Http\Controllers\Dev\MemberController as DevMemberController;
use App\Http\Controllers\Dev\AkunMT5Controller as DevAkunMT5Controller;
use App\Http\Controllers\Dev\WithdrawController as DevWithdrawController;
use App\Http\Controllers\Dev\ActivationaccountController as DevActivationaccountController;
use App\Http\Controllers\Dev\versionapp\VersionController as DevVersionController;
use App\Http\Controllers\Dev\BypassController as DevBypassController;
use App\Http\Controllers\Dev\NetworkController as DevNetworkController;
use App\Http\Controllers\Dev\KeuntunganController as DevKeuntunganController;
//Customer
use App\Http\Controllers\Customer\robot\RobotController as CustomerRobotController;
use App\Http\Controllers\Customer\robot\BeliRobotController as CustomerBeliRobotController;
use App\Http\Controllers\Customer\profile\ProfileController as CustomerProfileController;

//cashier
use App\Http\Controllers\Cashier\masterdata\UserController as CashierUserController;
use App\Http\Controllers\Cashier\topup\TopupController as CashierTopupController;
use App\Http\Controllers\Cashier\WithdrawController as CashierWithdrawController;
use App\Http\Controllers\Cashier\keuntungan\KeuntunganController as CashierKeuntunganController;
use App\Http\Controllers\Cashier\manajemenuser\AkunMT5Controller as CashierAkunMT5Controller;
use App\Http\Controllers\Cashier\ActivationaccountController as CashierActivationaccountController;
use App\Http\Controllers\Cashier\TopupController as CashierTopup2Controller;
use App\Http\Controllers\Cashier\mutasi\MutasiController as CashierMutasiController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingpage.maintenance');
});

Route::get('/test', function () {
    return view('page.dashboard.index');
});
// Route::get('/', function () {
//     return view('auth.login');
// })->middleware(['guest']);


Route::get('/csrf', function () {
    return csrf_token();
});


Route::get('/coomingsoon', function () {
    return view('coomingsoon');
})->name('cooming');

Route::get('/home', function () {
    Session::put('menu', 'home');
    return redirect('/dashboard/home');
});

Route::webhooks('webhook-moota-url');

Route::name('dashboard.')->prefix('dashboard')->middleware(['auth'])->group(function () {
    Route::get('/home', function () {
        return view('page.dashboard.index');
    });
    Route::name('dev.')->prefix('dev')->middleware(['role:Super Admin'])->group(function () {
        //mutasi
        Route::get('/mutasi', [DevMutationController::class, 'index'])->name('mutasi.index');
        Route::post('/mutasi/cek', [DevMutationController::class, 'find'])->name('mutasi.index.find');
        Route::post('/mutasi/usercek', [DevMutationController::class, 'usercek'])->name('mutasi.index.usercek');
        Route::post('/mutasi/store', [DevMutationController::class, 'store'])->name('mutasi.index.store');
        Route::get('/mutasi/history', [DevMutationController::class, 'history'])->name('mutasi.index.history');

        //activation Account
        Route::get('/activation-account', [DevActivationaccountController::class, 'index'])->name('activation-account.index');
        Route::get('/activation-account/approval', [DevActivationaccountController::class, 'approval'])->name('activation-account.approval');
        Route::get('/activation-account/approve{id}', [DevActivationaccountController::class, 'approve'])->name('activation-account.approve');

        //user
        Route::get('/user', [DevUserController::class, 'index'])->name('user.index');
        Route::get('/user/create', [DevUserController::class, 'create'])->name('user.create');
        Route::post('/user/store', [DevUserController::class, 'store'])->name('user.store');
        Route::post('/user/update', [DevUserController::class, 'update'])->name('user.update');
        Route::get('/user/delete{id}', [DevUserController::class, 'destroy'])->name('user.delete');
        Route::post('/user/profil', [DevUserController::class, 'profil'])->name('user.profil');
        Route::post('/user/password', [DevUserController::class, 'password'])->name('user.password');
        //member
        Route::get('/member', [DevMemberController::class, 'index'])->name('member.index');;
        Route::post('/member/store', [DevMemberController::class, 'store'])->name('member.store');
        Route::post('/member/update', [DevMemberController::class, 'update'])->name('member.update');
        Route::get('/member/delete{id}', [DevMemberController::class, 'destroy'])->name('member.delete');
        //akunMT5
        Route::get('/akunmt5', [DevAkunMT5Controller::class, 'index'])->name('akunmt5.index');
        Route::post('/akunmt5/store', [DevAkunMT5Controller::class, 'store'])->name('akunmt5.store');
        Route::post('/akunmt5/update', [DevAkunMT5Controller::class, 'update'])->name('akunmt5.update');
        //Bank
        Route::get('/bank', [DevBankController::class, 'index'])->name('bank.index');
        Route::post('/bank/store', [DevBankController::class, 'store'])->name('bank.store');
        Route::post('/bank/update', [DevBankController::class, 'update'])->name('bank.update');
        Route::get('/bank/delete{id}', [DevBankController::class, 'destroy'])->name('bank.delete');

        //level
        Route::get('/level', [DevLevelController::class, 'index'])->name('level.index');
        Route::post('/level/store', [DevLevelController::class, 'store'])->name('level.store');
        Route::post('/level/update', [DevLevelController::class, 'update'])->name('level.update');
        Route::get('/level/delete{id}', [DevLevelController::class, 'destroy'])->name('level.delete');
        //level
        Route::get('/robot', [DevRobotController::class, 'index'])->name('robot.index');
        Route::post('/robot/store', [DevRobotController::class, 'store'])->name('robot.store');
        Route::post('/robot/update', [DevRobotController::class, 'update'])->name('robot.update');
        Route::get('/robot/delete{id}', [DevRobotController::class, 'destroy'])->name('robot.delete');

        //Kurs
        Route::get('/kurs', [DevKursController::class, 'index'])->name('kurs.index');
        Route::post('/kurs/store', [DevKursController::class, 'store'])->name('kurs.store');
        Route::post('/kurs/update', [DevKursController::class, 'update'])->name('kurs.update');
        Route::get('/kurs/delete{id}', [DevKursController::class, 'destroy'])->name('kurs.delete');

        //Contact
        Route::post('/contact/update', [DevContactController::class, 'update'])->name('contact.update');
        Route::get('/contact', [DevContactController::class, 'index'])->name('contact.index');

        //Topup
        Route::get('/topup', [DevTopupController::class, 'index'])->name('topup.index');
        Route::get('/topup/approval', [DevTopupController::class, 'approval'])->name('topup.approval');
        Route::post('/topup/approve', [DevTopupController::class, 'approve'])->name('topup.approve');
        Route::get('/topupmt5/', [DevTopupController::class, 'indexmt5'])->name('topupmt5.index');
        Route::post('/topupmt5/tambah', [DevTopupController::class, 'tambah'])->name('topupmt5.tambah');
        Route::post('/topupmt5/tambahmodal', [DevTopupController::class, 'tambahmodal'])->name('topupmt5.tambahmodal');
        // Route::get('/topupmt5/kurang', [DevTopupController::class, 'tambah'])->name('topup.approve');
        // Route::post('/topupmt5/kurang{id}', [DevTopupController::class, 'approve'])->name('topup.approve');

        //WD
        Route::get('/withdraw', [DevWithdrawController::class, 'index'])->name('withdraw.index');
        Route::get('/withdraw/approval', [DevWithdrawController::class, 'approval'])->name('withdraw.approval');
        Route::post('/withdraw/approve', [DevWithdrawController::class, 'approve'])->name('withdraw.approve');

        //u=member bank
        Route::get('/member-bank', [DevMemberBankController::class, 'index'])->name('member-bank.index');
        Route::post('/member-bank/store', [DevMemberBankController::class, 'store'])->name('member-bank.store');
        Route::post('/member-bank/update', [DevMemberBankController::class, 'update'])->name('member-bank.update');
        Route::get('/member-bank/delete{id}', [DevMemberBankController::class, 'destroy'])->name('member-bank.delete');

        //version app
        Route::get('version-app', [DevVersionController::class, 'index'])->name('version.index');
        Route::post('version-app', [DevVersionController::class, 'store'])->name('version.store');

        //bypass robot
        Route::get('bypassrobot', [DevBypassController::class, 'index'])->name('bypassrobot.index');

        //networkt
        Route::get('network', [DevNetworkController::class, 'index'])->name('network.index');

        //keuntungan MT5

        Route::resources([
            'keuntungan' => DevKeuntunganController::class,
        ]);
        Route::get('/keuntungan/generate/{id}', [DevKeuntunganController::class, 'generate'])->name('keuntungan.generate');
    });
    Route::name('owner.')->prefix('owner')->middleware(['role:Admin'])->group(function () {
    });
    Route::name('directure.')->prefix('directure')->middleware(['role:Owner'])->group(function () {
    });
    Route::name('manager.')->prefix('manager')->middleware(['role:Leader'])->group(function () {
    });
    Route::name('cashier.')->prefix('cashier')->middleware([])->group(function () {

        //mutasi
        Route::get('/mutasi', [CashierMutasiController::class, 'index'])->name('mutasi.index');
        Route::post('/mutasi/cek', [CashierMutasiController::class, 'find'])->name('mutasi.index.find');
        Route::post('/mutasi/usercek', [CashierMutasiController::class, 'usercek'])->name('mutasi.index.usercek');
        Route::post('/mutasi/store', [CashierMutasiController::class, 'store'])->name('mutasi.index.store');
        Route::get('/mutasi/history', [CashierMutasiController::class, 'history'])->name('mutasi.index.history');

        Route::name('masterdata.')->prefix('/master-data')->group(function () {
            Route::get('/user', [CashierUserController::class, 'index'])->name('user.index');
        });
        Route::name('manajemenuser.')->prefix('/manajemen-user')->group(function () {
            Route::get('/akunmt5', [CashierAkunMT5Controller::class, 'index'])->name('akunmt5.index');
        });
        Route::name('topup.')->prefix('/topup')->group(function () {
            Route::get('/topupmt5/', [CashierTopupController::class, 'indexmt5'])->name('topupmt5.index');
            Route::post('/topupmt5/tambah', [CashierTopupController::class, 'tambah'])->name('topupmt5.tambah');
            Route::post('/topupmt5/tambahmodal', [CashierTopupController::class, 'tambahmodal'])->name('topupmt5.tambahmodal');
        });
        Route::resources([
            'keuntungan' => CashierKeuntunganController::class,
        ]);
        Route::get('/keuntungan/generate/{id}', [CashierKeuntunganController::class, 'generate'])->name('keuntungan.generate');

        //activation Account
        Route::get('/activation-account', [CashierActivationaccountController::class, 'index'])->name('activation-account.index');
        Route::get('/activation-account/approval', [CashierActivationaccountController::class, 'approval'])->name('activation-account.approval');
        Route::get('/activation-account/approve{id}', [CashierActivationaccountController::class, 'approve'])->name('activation-account.approve');

        //Topup
        Route::get('/topup', [CashierTopup2Controller::class, 'index'])->name('topup.index');
        Route::get('/topup/approval', [CashierTopup2Controller::class, 'approval'])->name('topup.approval');
        Route::post('/topup/approve', [CashierTopup2Controller::class, 'approve'])->name('topup.approve');

        //WD
        Route::get('/withdraw', [CashierWithdrawController::class, 'index'])->name('withdraw.index');
        Route::get('/withdraw/approval', [CashierWithdrawController::class, 'approval'])->name('withdraw.approval');
        Route::post('/withdraw/approve', [CashierWithdrawController::class, 'approve'])->name('withdraw.approve');
    });
    Route::name('customer.')->prefix('customer')->middleware(['role:Customer'])->group(function () {

        Route::resources([
            'list-robot' => CustomerRobotController::class,
            'beli-robot' => CustomerBeliRobotController::class,
            'profile' => CustomerProfileController::class,
        ]);
        Route::post('beli-robot/belirobot', [CustomerBeliRobotController::class, 'belirobot'])->name('beli-robot.belirobot');
    });
});
